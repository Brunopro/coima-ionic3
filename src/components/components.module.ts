import { NgModule } from '@angular/core';
import { ExpandableComponent } from './expandable/expandable';
@NgModule({
    declarations:[ExpandableComponent],
    exports:[ExpandableComponent]
})
export class ComponentsModule {}
