export interface IUser{
    uid?: string;
    name?: string;
    lastname?: string,
    admin?:number,
    phone?: string,
    cpf?: string,
    email: string;
    password?: string;
    password_confirmation?:string,
    company?: string
  }