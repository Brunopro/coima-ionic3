export interface IEnquadramento {
    id?:number;
    codigo: number;
    descricao: string;
    desdobramento: number
} 