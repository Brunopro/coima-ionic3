export interface IMulta {
    id?:number;
    multa_id?:number;
    enviado?:number;
    agentes_id:number;
    enquadramento_id: number;
    estado_id?:number;
    status?:number;
    uf_emplacamento_id?: number;
    municipio_emplacamento_id: number;
    uf_emplacamento?: string;
    created?: string;
    agente_nome?: string;
    data_infracao: string;
    data_infracao_br?: string;
    descricao_marca_modelo?: string;
    enquadramento_codigo?: string;
    enquadramento_descricao?: string;
    enquadramento_desdobramento?: string;
    foto_infracao?: string;
    hora_infracao: string;
    local_ocorrencia: string;
    municipio_emplacamento_nome?: string;
    numero_auto_infracao: string;
    observacao: string;
    placa: string;
    expanded?: boolean;
}