import { Component } from '@angular/core';
import { IonicPage, NavParams, Loading, LoadingController } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { ToastController, AlertController } from 'ionic-angular';
import moment from 'moment'
import { AngularFireDatabase } from 'angularfire2/database';
import { PreferencesProvider } from '../../providers/preferences/preferences';


@IonicPage()
@Component({
  selector: 'page-print-multa',
  templateUrl: 'print-multa.html',
})
export class PrintMultaPage {

  multa: any;
  pairedList: pairedlist;
  listToggle: boolean = false;
  pairedDeviceID: number = 0;
  dataSend: string;
  load: Loading;
  company_description:any;

  constructor(
    public navParans: NavParams,
    public preferencesProv: PreferencesProvider,
    private bluetoothSerial: BluetoothSerial,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private afd: AngularFireDatabase,
  ) {
    this.multa = this.navParans.get('multa');
    console.log('multa', this.multa)
    this.checkBluetoothEnabled();
   }

   ionViewDidLoad(){
    this.preferencesProv.get('user').then((user) =>  {
      this.afd.database.ref('prefecture').once('value', (snapshot) => {
        Object.keys(snapshot.val()).map(key => {
          let prefecture = snapshot.val();
          console.log('user company', user.company)
          if(user.company ==  prefecture[key]['name']){
            this.company_description = prefecture[key]['description'];
            this.createCupom();
          }
        });
      })
    });
   }

  createCupom(){

    let data_split = this.multa["data_infracao"].split(" ");

    data_split = data_split[0];

    //formatando data para padrão brasileiro
    let data_multa_br = moment(data_split, 'YYYY-MM-DD').format('DD/MM/YYYY'); 

    if(this.multa["uf_emplacamento"]){
      this.dataSend =
      "-------------------------------- \n" + 
      "              COIMA \n" + 
      this.company_description + "\n" + 
      "         AUTO INFRAÇÃO  \n" + 
      "-------------------------------- \n" + 
      "NÚMERO AUTO: "+ this.multa["numero_auto_infracao"].toUpperCase() + "\n" +
      "PLACA: "+ this.multa["placa"] + "\n" + 
      "DESCRIÇÃO: "+ this.multa["enquadramento_dados"]["descricao"].toUpperCase()+ "\n" +
      "AGENTE: "+ this.multa["agente_codigo"] + "\n" +
      "LOCAL: "+ this.multa["local_ocorrencia"].toUpperCase()+ "\n" +
      "ESTADO EMPLACAMENTO: "+ this.multa["uf_emplacamento"]["uf"] + "\n" +
      "MUNÍCIPIO EMPLACAMENTO: "+ this.multa["municipio_emplacamento_nome"] + "\n" +
      "DATA: "+ data_multa_br + "\n" +
      "HORA: "+ this.multa["hora_infracao"] + "\n" +
      "-------------------------------- \n \n" +
      "       ___________________ \n" + 
      "        ASSINATURA AGENTE \n \n" +
      "-------------------------------- \n" +
      "       ___________________ \n" + 
      "       ASSINATURA CONDUTOR\n \n" +
      "-------------------------------- \n \n";
    } else {

    this.dataSend =
    "-------------------------------- \n" + 
    "              COIMA \n" + 
    this.company_description + "\n" + 
    "         AUTO INFRAÇÃO  \n" + 
    "-------------------------------- \n" + 
    "NÚMERO AUTO: "+ this.multa["numero_auto_infracao"].toUpperCase() + "\n" +
    "PLACA: "+ this.multa["placa"] + "\n" + 
    "DESCRIÇÃO: "+ this.multa["enquadramento_dados"]["descricao"].toUpperCase()+ "\n" +
    "AGENTE: "+ this.multa["agente_codigo"] + "\n" +
    "LOCAL: "+ this.multa["local_ocorrencia"].toUpperCase()+ "\n" +
    "DATA: "+ data_multa_br + "\n" +
    "HORA: "+ this.multa["hora_infracao"] + "\n" +
    "-------------------------------- \n \n" +
    "       ___________________ \n" + 
    "            ASSINATURA \n \n" +
    "-------------------------------- \n" +
    "       ___________________ \n" + 
    "            ASSINATURA \n \n" +
    "-------------------------------- \n \n";

    }

    console.log("dataSend", this.dataSend)

  }


  checkBluetoothEnabled() {
    this.bluetoothSerial.isEnabled().then(success => {
      this.listPairedDevices();
    }, error => {
      this.showError("Por favor, ative o bluetooth")
    });
  }

  listPairedDevices() {
    this.bluetoothSerial.list().then(success => {
      console.log(success)
      this.pairedList = success;
      this.listToggle = true;
    }, error => {
      this.showError("Por favor, ative o bluetooth")
      this.listToggle = false;
    });
  }

  connect(address) {
    this.loadPresent()
    // Attempt to connect device with specified address, call app.deviceConnected if success
    this.bluetoothSerial.connect(address).subscribe(success => {
      //this.deviceConnected();
      this.sendData()
    }, error => {
      this.load.dismiss()
      this.showError("Erro ao tentar se conectar com o dispositivo");
    });
  }

  deviceConnected() {
    // Subscribe to data receiving as soon as the delimiter is read
    this.bluetoothSerial.subscribe('\n').subscribe(success => {
      this.handleData(success);
      this.sendData()
      //this.showToast("Conexão estabelecida");
    }, error => {
      this.showError(error);
    });
  }

  deviceDisconnected() {
    // Unsubscribe from data receiving
    this.bluetoothSerial.disconnect();
  }

  handleData(data) {
    this.showToast(data);
  }

  sendData() {
    this.bluetoothSerial.write(this.dataSend).then(success => {
      this.load.dismiss()
      this.showToast("Imprimido com sucesso!");
      this.deviceDisconnected()
    }, error => {
      this.load.dismiss()
      this.deviceDisconnected()
      this.showError("Ocorreu um erro ao tentar imprimir")
    });
  }

  async showError(error) {
    let alert = await this.alertCtrl.create({
      title: 'Erro',
      subTitle: error,
      buttons: ['Ok']
    });
    alert.present();
  }

  async showToast(msj) {
    const toast = await this.toastCtrl.create({
      message: msj,
      duration: 3000
    });
    toast.present();

  }

  loadPresent(){
    this.load = this.loadingCtrl.create({
      content: "Aguarde..."
    })

    this.load.present()
  }
}

interface pairedlist {
  "class": number,
  "id": string,
  "address": string,
  "name": string
}

