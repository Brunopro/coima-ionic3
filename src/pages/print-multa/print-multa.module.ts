import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrintMultaPage } from './print-multa';

@NgModule({
  declarations: [
    PrintMultaPage,
  ],
  imports: [
    IonicPageModule.forChild(PrintMultaPage),
  ],
})
export class PrintMultaPageModule {}
