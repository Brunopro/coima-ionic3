import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ValidatorProvider } from '../../providers/validator/validator';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

 
  showPasswordText = false;
  signUpForm: FormGroup;

  load: Loading;
  message: string = ''

  constructor(
    public navCtrl: NavController,
    public formBuilder:FormBuilder,
    public validatorProv: ValidatorProvider,
    public authProv: AuthProvider,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController) {

      let cpfIsValid = (control: FormControl) => {
        //vai verificar se o cpf é valido, se não for retorna um {"cpf_invalid": true} e o campo fica invalido
        if (validatorProv.validatorCpf(control.value)) {
          return null
        } else {
          return { "cpf_invalid": true }
        }
      }

      let nameRegex = '^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]+$'
  
      this.signUpForm = formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(2), Validators.pattern(nameRegex)]],
        lastname: ['', [Validators.required, Validators.minLength(2), Validators.pattern(nameRegex)]],
        cpf: ['', Validators.compose([Validators.required, Validators.minLength(14), cpfIsValid])],
        phone: ['', Validators.compose([Validators.required, Validators.minLength(15)])],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]]
      })
      
  }

  console(){
    if(this.signUpForm.invalid == false){

        let data = {
        name: this.signUpForm.controls.name.value,
        lastname: this.signUpForm.controls.lastname.value,
        cpf: this.signUpForm.controls.cpf.value,
        phone: this.signUpForm.controls.phone.value,
        email: this.signUpForm.controls.email.value,
        password: this.signUpForm.controls.password.value,
        company: ""
      }

      this.navCtrl.push('RegisterVehiclePage', { signUpForm: data})
 
    } else {
      console.log('verifique se todos os campos foram preenchidos corretamente')
    }
  }



  alertPresent(title, message){
    let alert = this.alertCtrl.create({
      title: title,
      message: message
    })

    alert.present()
    
  }

}
