import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading, Platform } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WebServiceSoapMaProvider } from '../../providers/web-service-soap-ma/web-service-soap-ma';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { OverlayProvider } from '../../providers/overlay/overlay';


@IonicPage()
@Component({
  selector: 'page-consulta-veiculo',
  templateUrl: 'consulta-veiculo.html',
})
export class ConsultaVeiculoPage {

  public query_types = [{id:2, name:'Placa'},{id:1, name:'Chassi'},{id:5, name:'Renavam'}];
  public toppings = {
    id: '',
    name: ''
  };
  public search_value:string = " ";
  public retorno:any;
  public formCondutor:FormGroup;
  private load:Loading;
  private user:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder:FormBuilder,
    public alertCtrl: AlertController,
    private webService:WebServiceSoapMaProvider,
    private loadingCtrl: LoadingController,
    private platform: Platform,
    private preferenceProv: PreferencesProvider,
    private overlayProv: OverlayProvider
    ) {

      this.platform.ready().then(() => {
        this.preferenceProv.get('user').then((user) => {
          this.user ={
            cpf: user.cpf.replace(".", "").replace(".", "").replace("-", ""),
            company: user.company
          } 
        })
      });

      this.formCondutor = formBuilder.group({
        tipo: ['', Validators.required],
        chave: ['', Validators.required]
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsultaCondutorPage');
  }

  
  onSubmit(){

    this.load = this.loadingCtrl.create({
      content: 'aguarde..'
    })

    this.load.present()

    

    console.log(this.formCondutor.get('chave').value['name'])
    if(this.formCondutor.valid == false){
      console.log('if', this.formCondutor.value)
      this.alertCtrl.create({
        message: 'Preencha os campos corretamente'
      }).present()
      this.load.dismiss();
      return
    }

    this.webService.consulta({data:this.formCondutor.value, tipo: '1', user:this.user}).subscribe((res) => {
      console.log(res)
      this.filterResult(res);
      this.load.dismiss();
    }, err => {
      this.overlayProv.alertPresent({title: 'Informativo', message: 'Servidor do Detran-MA, indisponível no momento.'})
      this.load.dismiss();
    })

    console.log('passou no teste', this.formCondutor.valid);
  }

  public changeType(){
    console.log('change value', this.formCondutor.controls.tipo.value);

    this.formCondutor.controls.chave.setValue('');


  }

  private filterResult(result):void {

    if(result['WS_RETORNO'] == 'L001'){
      this.filterL001(result)
    } else{
      this.filterN001(result)
    } 
  }

  private filterL001(result):void{
    this.retorno = {
      mensagem: result['WS_MENSAGEM'],
      data: [
      {title: 'PLACA', data: result['WS_PLACA'] == ""? "NÃO INFORMADO" : result['WS_PLACA']},
      {title: 'PROPRIETARIO', data: result['WS_PROPRIETARIO'] == ""? "NÃO INFORMADO" : result['WS_PROPRIETARIO']},
      {title: 'MARCA', data: result['WS_MARCA'] == ""? "NÃO INFORMADO" : result['WS_MARCA']},
      {title: 'Nº CHASSI', data: result['WS_CHASSI'] == ""? "NÃO INFORMADO" : result['WS_CHASSI']},
      {title: 'RENAVAM', data: result['WS_RENAVAM'] == ""? "NÃO INFORMADO" : result['WS_RENAVAM']},
      {title: 'TIPO', data: result['WS_TIPO'] == ""? "NÃO INFORMADO" : result['WS_TIPO']},
      {title: 'ANO FABRICAÇÃO', data: result['WS_ANOFAB'] == ""? "NÃO INFORMADO" : result['WS_ANOFAB']},
      {title: 'ANO MODELO', data: result['WS_ANOMOD'] == ""? "NÃO INFORMADO" : result['WS_ANOMOD']},
      {title: 'CARROCERIA', data: result['WS_CARROCERIA'] == ""? "NÃO INFORMADO" : result['WS_CARROCERIA']},
      {title: 'CATEGORIA', data: result['WS_CATEGORIA'] == ""? "NÃO INFORMADO" : result['WS_CATEGORIA']},
      {title: 'COMBUSTÍVEL', data: result['WS_COMBUSTIVEL'] == ""? "NÃO INFORMADO" : result['WS_COMBUSTIVEL']},
      {title: 'COR', data: result['WS_COR']  == ""? "NÃO INFORMADO" : result['WS_COR']},
      {title: 'Nº CAIXA DE CAMBIO', data: result['WS_CXACAMBIO'] == ""? "NÃO INFORMADO" : result['WS_CXACAMBIO']},
      {title: 'INDICADOR DE DÉBITOS', data: result['WS_DEBITOS'] == true? 'Sim': 'Não'},
      {title: 'ÚLTIMA EMISSÃO DOCUMENTO', data: result['WS_EMISSAO'] == ""? "NÃO INFORMADO" : result['WS_EMISSAO']},
      {title: 'ESPÉCIE', data: result['WS_ESPECIE'] == ""? "NÃO INFORMADO" : result['WS_ESPECIE']},
      {title: 'Nº LACRE', data: result['WS_LACRE'] == ""? "NÃO INFORMADO" : result['WS_LACRE']},
      {title: 'Nº MOTOR', data: result['WS_NMOTOR'] == ""? "NÃO INFORMADO" : result['WS_NMOTOR']},
      {title: 'UF EMPLACAMENTO', data: result['WS_UF'] == ""? "NÃO INFORMADO" : result['WS_UF']},
      {title: 'RESTRIÇÕES', data: result['WS_RESTRICAO']['string']? result['WS_RESTRICAO']['string'] : 'SEM RESTRICÃO'}

    ]}
  }

  private filterN001(result):void {
    this.retorno = null;
    this.alertPresent({message:result['WS_MENSAGEM']})
  }

  alertPresent(params:{title?:string, message:string}):void {
    this.alertCtrl.create({
      title: params.title?params.title:'Falha',
      message: params.message,
      buttons: ['OK']
    }).present()
  }

}
