import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsultaVeiculoPage } from './consulta-veiculo';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    ConsultaVeiculoPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsultaVeiculoPage),
    BrMaskerModule
  ],
})
export class ConsultaVeiculoPageModule {}
