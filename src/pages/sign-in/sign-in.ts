import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, AlertController, LoadingController, ToastController, MenuController, Platform } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { IUser } from '../../interfaces/IUser';
import { HttpClient } from '@angular/common/http';
import { SqliteHelperProvider } from '../../providers/sqlite-helper/sqlite-helper';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
import { MultasProvider } from '../../providers/multas/multas';
import { environment } from '../../environments/environments';
import { AppVersion } from '@ionic-native/app-version';

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {

  private storage: SQLite;

  showPasswordText = false;
  loginForm: FormGroup;

  load:Loading

  private url:string = environment.url;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder:FormBuilder,
    private authProv: AuthProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private preferencesProv: PreferencesProvider,
    private menuCtrl: MenuController,
    private http: HttpClient,
    private sqliteHelperProv: SqliteHelperProvider,
    private multasProv: MultasProvider,
    private platform: Platform,
    private appVersion: AppVersion) {

      /*
      appVersion.getVersionNumber().then((res) => {
        console.log('version', res)
      });
      
      http.get('https://play.google.com/store/apps/details?id=" + "br.com.coima" + "&hl=en"').subscribe((res) => {
        console.log('res play store', res)
      })
      */
      
      this.loginForm = formBuilder.group({
        email: [null, [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]]
      })
  }


  pushSignUp(){
    console.log('excute push')
    this.navCtrl.push('SignUpPage')
  }

  ionViewDidLoad(){
    //this.getDbAlreadyCreated()
  }

  signIn(){


    if(this.loginForm.valid){
      this.loadPresent()
      this.authProv.signIn(this.loginForm.controls.email.value, this.loginForm.controls.password.value).then((res) => {

        this.authProv.getDataUserInDatabase(res.user.uid).then((data) => {

          let user:IUser = {
            uid: res.user.uid,
            name: data.name,
            lastname: data.lastname,
            email: data.email,
            phone: data.phone,
            cpf: data.cpf,
            admin: data.admin? data.admin: 0,
            company: data.company
          }

          //user.cpf = '53186094062';

          
          if(data.admin == 1){
            this.enableAuthenticatedMenu('admin')
          } else {
            this.enableAuthenticatedMenu('users')
          }
         
          if(data.admin == 1){
            this.getDbAlreadyCreated().then(() => {
              this.SqliteGravarDadosApi(user.cpf.replace(".", "").replace(".", "").replace("-", ""), user.company).then(() => {
                this.authProv.getDataVehicleInDatabase(res.user.uid).then((vehicle) => {
                  this.preferencesProv.create('vehicle', vehicle).then(() => {
                    this.preferencesProv.create('user', user).then(() => {
                      this.load.dismiss();
                      this.multasProv.connection = true;
                      this.navCtrl.setRoot('HomePage');
                    })
                  });
                });
              }).catch((err) => {
                this.load.dismiss();
                this.alertPresent('Erro', 'ocorreu um erro ao concluir o login 513');
                console.log('erro', err )
              })
            }).catch(() => {
              this.load.dismiss();
              this.alertPresent('Erro', 'ocorreu um erro ao concluir o login 514');
            })
          } else {
            this.authProv.getDataVehicleInDatabase(res.user.uid).then((vehicle) => {
              this.preferencesProv.create('vehicle', vehicle).then(() => {
                this.preferencesProv.create('user', user).then(() => {
                  this.load.dismiss();
                  this.navCtrl.setRoot('HomePage');
                })
              });
            });
          }

        })  
      }).catch((err) => {
        this.load.dismiss();
        if (err.code == 'auth/invalid-email') {
          this.alertPresent('Erro', 'O endereço de email é inválido')
        } else if (err.code == 'auth/user-disabled') {
          this.alertPresent('Erro', ' Email fornecido desativado.')
        } else if (err.code == 'auth/user-not-found') {
          this.alertPresent('Erro', 'Não existe usúario correspodente ao email fornecido.')
        } else if (err.code == 'auth/wrong-password') {
          this.alertPresent('Erro', 'Email ou senha inválidos')
        } else if(err.code == 'auth/network-request-failed'){
          this.alertPresent('Erro', 'Verifique sua conexão a internet e tente novamente')
        } else {
          this.alertPresent('Erro', 'falha ao tentar logar com os dados atuais')
        } 
      })
    } else {
      this.alertPresent('Erro', 'Preencha os campos corretamente')
    }

  }

  forgetPassword() {
    let alert = this.alertCtrl.create({
      title: 'Esqueceu sua senha?',
      message: 'Digite o endereço do seu email para enviarmos o link de redefinição de senha.',
      inputs: [
        {
          type: 'email',
          name: 'email',
          placeholder: 'Email'
        }
      ],
      buttons: [
        {
          text: 'CANCELAR',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'ENVIAR',
          handler: data => {
            this.authProv.resetPassword(data.email).then(()=>{
              this.toastPresent('Recuperação de senha iniciada. Email enviado!')
            }).catch((err: any) => {
              if(err.code == 'auth/invalid-email'){
                this.toastPresent('O email digitado é inválido!')
              } if(err.code == 'auth/user-not-found'){
                this.toastPresent('Nenhum usuário com esse email cadastrado!')
              } else {
                this.alertPresent(err.code, err.message)
              }
            })
          }
        }
      ]
    });
    alert.present();
  }

  private SqliteGravarDadosApi(cpf:string,company:string):Promise<any> {
    

    let body = {
      user: {
        cpf: cpf,
        company: company
      }
    }

    console.log('getalldata', body)
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.url + '/auth/getalldata', body).subscribe((res) => {
        console.log('res aqui', res)
        if(res["Error"]){
          this.load.dismiss();
          console.log("aqui")
        } else {
            let enquadramentos_agentes_insert_query = [];
    
            for(let i = 0; i < res['enquadramentos']["length"]; i++) {
              enquadramentos_agentes_insert_query.push(['INSERT INTO enquadramentos (id,codigo,descricao, desdobramento) VALUES (?,?,?,?)', [res['enquadramentos']['data'][i]['id'], res['enquadramentos']['data'][i]['codigo'], res['enquadramentos']['data'][i]['descricao'],  res['enquadramentos']['data'][i]['desdobramento']]]);
            }
    
            for(let i = 0; i < res['agentes']["length"]; i++) {
              enquadramentos_agentes_insert_query.push(['INSERT INTO agentes (id,nome,cpf) VALUES (?,?,?)', [res['agentes']['data'][i]['id'], res['agentes']['data'][i]['nome'], res['agentes']['data'][i]['cpf']]]);
            }
    
            this.sqliteHelperProv.getDb('dynamicbox.db', true)
            .then((db: SQLiteObject) => {
              
              db.sqlBatch([
                [ 'CREATE TABLE IF NOT EXISTS enquadramentos (id INTEGER PRIMARY KEY,codigo INTEGER, descricao TEXT, desdobramento INTEGER)', [] ],
                [ 'CREATE TABLE IF NOT EXISTS agentes (id INTEGER PRIMARY KEY, nome TEXT, cpf TEXT)', [] ],
                [ 'CREATE TABLE IF NOT EXISTS multas (' +
                'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,' + 
                'multa_id INTEGER,' +
                'enviado INTEGER, ' +
                'agentes_id INTEGER,' +
                'enquadramento_id INTEGER,' +
                'estado_id INTEGER,' +
                'status INTEGER,' +
                'uf_emplacamento_id INTEGER,' +
                'municipio_emplacamento_id INTEGER,' +
                'uf_emplacamento TEXT,' +
                'created TEXT,' + 
                'agente_nome TEXT,' + 
                'data_infracao TEXT,' + 
                'data_infracao_br TEXT,' + 
                'descricao_marca_modelo TEXT,' + 
                'enquadramento_codigo TEXT,' + 
                'enquadramento_descricao TEXT,' + 
                'enquadramento_desdobramento TEXT,' + 
                'foto_infracao TEXT,' + 
                'hora_infracao TEXT,' + 
                'local_ocorrencia TEXT,' + 
                'municipio_emplacamento_nome TEXT,' + 
                'numero_auto_infracao TEXT,' + 
                'observacao TEXT,' + 
                'placa TEXT)', []]
              ])
              .then(success => {
                db.sqlBatch(enquadramentos_agentes_insert_query)
                .then(() => {
                  resolve('sucess');
                  console.log('sqlite table enquadramentos and agentes alimentadas')
                })
                .catch((e: Error) => {
                  reject('error');
                  console.log('Error ao alimentar tables enquadramentos e agentes', e);
                });
              })
              .catch(err => {
                reject('error');
                console.error('Error creating multiples tables!', err);
              });

            })// fim then
            
        }
      }, err => {
        reject('error');
        console.log('ocorreu um erro', err)
      }); //fim subscribe

    });//fim promisse
    
  }//fim function

  private getDbAlreadyCreated():Promise<any> {
    return this.platform.ready().then(() => {
      this.storage = new SQLite();
      return this.storage.create({ name: "sqlite.db", location: 'default', createFromLocation: 1 })
    });
  }
  


  
  enableAuthenticatedMenu(user:string) {
    if(user == 'users'){
      this.menuCtrl.enable(true, 'users');
      this.menuCtrl.enable(false, 'admin');
    } else {
      this.menuCtrl.enable(false, 'users');
      this.menuCtrl.enable(true, 'admin');
    }
  }

  loadPresent(){
    this.load = this.loadingCtrl.create({
      content: 'checando dados, aguarde...'
    })

    this.load.present()
  }

  alertPresent(title, message){
    let alert = this.alertCtrl.create({
      title: title,
      message: message
    })

    alert.present()
  }

  toastPresent(message){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
    })

    toast.present()
  }

}
