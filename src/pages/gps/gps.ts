import { Component, } from '@angular/core';
import { NavController, LoadingController, Loading, Platform, AlertController , IonicPage, NavParams} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { Diagnostic } from '@ionic-native/diagnostic';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
} from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-gps',
  templateUrl: 'gps.html',
})
export class GpsPage {

  map: GoogleMap;
  lat: any; long: any;


  marker: any;

  inital_position = {
    lat: 0,
    lng: 0
  }

  geoLatitude: number;
  geoLongitude: number;
  geoAccuracy: number;
  geoAddress: string;


  //Geocoder configuration
  geoencoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };

  mapOptions: GoogleMapOptions;

  callback: any;


  //para pedir permições
  gotPosition: boolean = false;

  gaveLocationPermission: boolean = false;
  hasEnabledLocation: boolean = false;
  doneChecking: boolean = false;
  hasRequestedLocation: boolean = false;

  constructor(
    public geolocation: Geolocation,
    public navCtrl: NavController,
    public navParams: NavParams,
    public nativeGeocoder: NativeGeocoder,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public diagnostic: Diagnostic,
    public alertCtrl: AlertController) {

      //recuperando callback
      this.callback = this.navParams.get("callback")

      this.getLocation()
  }


  currentPosition() {
    //start load

    let load = this.loadingPresent()

    this.geolocation.getCurrentPosition().then(res => {
      if(res) {
        this.inital_position = {
          lat: res.coords.latitude,
          lng: res.coords.longitude
        }
        console.log("success geolocation", this.inital_position)
        this.loadGoogleMap();
      } else {
        //Não foi possivel recuperar os dados da sua localização
        console.log("Não foi possivel recuperar os dados da sua localização")
        load.dismiss()
      }
    })
  }

  loadGoogleMap() {
    console.log('loading mapa')
    let mapLoader = this.loadingCtrl.create({
      content: 'Carregando mapa'
    });

    mapLoader.present()

    let mapOptions = {
      camera: {
        target: {
          lat:  this.inital_position.lat,
          lng: this.inital_position.lng
        },
        zoom: 18,
        tilt: 30
      }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('this.map.one(GoogleMapsEvent.MAP_READY).then()')
        //recupear endereço
        this.getGeoencoder(this.inital_position.lat, this.inital_position.lng)

        this.map.addMarker({
          title: 'Localização atual',
          icon: 'red',
          animation: 'DROP',
          position: {
            lat: this.inital_position.lat,
            lng: this.inital_position.lng
          }
        })
          .then(marker => {
            console.log('then setando marker', marker)

            this.marker = marker
            mapLoader.dismiss()

            //marker.trigger(GoogleMapsEvent.MARKER_CLICK, marker.getPosition());
          });

      });

    let position = {
      lat: "",
      lng: ""
    }

    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((res) => {

      if (this.marker) {
        this.marker.destroy()
        console.log('destroy marker')
        console.log('create new marker', res[0]['lat'], res[0]['lng'])

        this.getGeoencoder(res[0]['lat'], res[0]['lng'])
        this.map.addMarker({
          icon: 'red',
          animation: 'DROP',
          position: {
            lat: res[0]['lat'],
            lng: res[0]['lng']
          }



        }).then((marker) => {
          this.marker = marker
          console.log("setando new marker", marker)
        })

      }

    })
  }


  //geocoder method to fetch address from coordinates passed as arguments
  getGeoencoder(latitude, longitude) {
    console.log('getGeoencoder', latitude, longitude)
    this.nativeGeocoder.reverseGeocode(latitude, longitude, this.geoencoderOptions)
      .then((result: NativeGeocoderReverseResult[]) => {
        console.log('result nativeGeocoder', result)
        this.geoAddress = this.generateAddress(result[0]);

        //Endereço completo da localidade
        this.geoAddress
      })
      .catch((error: any) => {
        alert('Error getting location' + JSON.stringify(error));
        console.log('error native geocoder', error)
      });
  }

  //Return Comma saperated address
  generateAddress(addressObj) {
    console.log('addressObj', addressObj)
    let obj = [];
    let address = "";
    /*
    if(addressObj['subAdministrativeArea'] != ""){
      obj.push(addressObj['subAdministrativeArea']);
    }
    */
    if(addressObj['subLocality'] != ""){
      obj.push(addressObj['subLocality']);
    }
    if(addressObj['thoroughfare'] != ""){
      obj.push(addressObj['thoroughfare']);
    }
    
    obj.reverse();
    for (let val in obj) {
      if (obj[val].length)
        address += obj[val] + ', ';
    }
    console.log("adress.slice Endereço completo:", address.slice(0, -2))
    return address.slice(0, -2);
  }

  onSubmit(){
    this.callback(this.geoAddress).then(()=>{
        this.navCtrl.pop();
    });
  }

  loadingPresent(): Loading {
    let load = this.loadingCtrl.create({
      content: "Carregando mapa.."
    })
    load.present()

    return load;
  }

  alertPresent(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      message: message
    })

    return alert.present();
  }


  getLocation() {


    let authorizationLoader = this.loadingCtrl.create({
      content: "Checando permissões..."
    });
    authorizationLoader.present();


    this.diagnostic.isLocationAuthorized().then((resp) => {
      authorizationLoader.dismiss().then(() => {
        this.gaveLocationPermission = resp;
        if (this.gaveLocationPermission) {
          let enablingLoader = this.loadingCtrl.create({
            content: "Checando status da localização..."
          });
          enablingLoader.present();
          console.log('Gave location permission')
          this.diagnostic.isLocationEnabled().then((resp) => {
            console.log('Location is enabled : ' +resp)
            enablingLoader.dismiss()
            console.log('Enabling loader dismissed')
            this.doneChecking = true;
            this.hasEnabledLocation = resp;
            if (this.hasEnabledLocation) {
              this.getPosition();
            }
          }).catch((e) => {
            console.log('isLocationEnabled()')
            enablingLoader.dismiss()
          })
        } else {
          this.gotPosition = false
          //this.resetUBSDistance()
          this.lat = null
          this.long = null
          if(!this.hasRequestedLocation) {
            this.hasRequestedLocation = true
            this.diagnostic.requestLocationAuthorization().then((res)=>{
              if(res!='denied'){
                console.log('Permission : '+ res)
                this.diagnostic.isLocationAuthorized().then((res)=>{
                  this.gaveLocationPermission = res;
                  if (this.gaveLocationPermission) {
                    let enablingLoader = this.loadingCtrl.create({
                      content: "Checando status da localização.."
                    });
                  enablingLoader.present();
                    this.diagnostic.isLocationEnabled().then((resp) => {
                      enablingLoader.dismiss()
                      this.doneChecking = true;
                      this.hasEnabledLocation = resp;
                      if (this.hasEnabledLocation) {
                        this.getPosition();
                      }
                    }).catch((e) => {
                      console.log('isLocationEnabled()')
                      enablingLoader.dismiss()
                    })
                  }
                })
              } else { // denied location

              }
            })
          }
          this.doneChecking = true;
        }
      });
    }, err =>{
      authorizationLoader.dismiss()
      this.gotPosition = false
      //this.resetUBSDistance()
      this.lat = null
      this.long = null
    });
  }

  getPosition() {
    let getPositionLoader = this.loadingCtrl.create({
      content: 'Checando localização...'
    })

    getPositionLoader.present()
    
    this.platform.ready().then(() => {
      this.geolocation.getCurrentPosition().then((resp) => {
        this.lat = resp.coords.latitude;
        this.long = resp.coords.longitude;
        this.gotPosition = true;

        this.inital_position.lat = resp.coords.latitude;
        this.inital_position.lng =resp.coords.longitude;
        
        getPositionLoader.dismiss()
        this.loadGoogleMap()


      }).catch((error) => {
        getPositionLoader.dismiss()
        console.log('Error getting location', error);
        this.alertPresent('Error', 'Não foi possivel encontrar dados da localização')  
      });
    });
  }



  


  
}
