import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ActionSheetController } from 'ionic-angular';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { VehicleProvider } from '../../providers/vehicle/vehicle';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-my-vehicle',
  templateUrl: 'my-vehicle.html',
})
export class MyVehiclePage {

  vehicles;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public preferencesProv: PreferencesProvider,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    private storage: Storage,
    public vehicleProv: VehicleProvider) {
      storage.ready().then(() => {
        this.vehicles =  this.preferencesProv.vehicle;
      });

    console.log(this.vehicles)
  }

  ionViewDidLoad() {
    
  }

  addVehiclePage(){
    this.navCtrl.push('AddVehiclePage')
  }

  openActionSheet(vehicle){
    let actionsheet = this.actionSheetCtrl.create({
    title:"Opções",
    buttons:[{
    text: 'Editar',
    icon: 'create',
    cssClass: 'EditionIcon',
    handler: () => {
      this.navCtrl.push('UpdateVehiclePage', {vehicle: vehicle})
    }
    },
    {
      text: 'Delete',
      icon: 'trash',
      cssClass: 'EditionIcon',
      handler: () => {
        if(this.vehicles.length > 1) {
        this.deleteConfirm(vehicle)
        } else {
          this.alertPresent('Alerta','A sua lista de veículos não pode ficar vazia');
        }
      }
    }]
    });
    actionsheet.present();
   }

   deleteConfirm(vehicle) {
    let confirm = this.alertCtrl.create({
      title: 'Confirmação',
      message: 'Tem certeza que quer deletar o dispositivo?',
      buttons: [
        {
          text: 'Sim',
          handler:() => {
            //trata a promisse e manda o data chamando o setMobileAtual
            this.vehicleProv.deleteVehicle(vehicle).then(() => {
              let index = this.preferencesProv.vehicle.indexOf(vehicle);
              this.preferencesProv.vehicle.splice(index, 1)
              this.preferencesProv.create('vehicle', this.preferencesProv.vehicle)
            }).catch((err) => {
              console.log('erro ao deletar', err);
              this.alertPresent('Alerta', 'Ocorreu um erro ao deletar veículo tente novamente')
            })
          }
        },  
        {
          text: 'Cancelar',
          handler:() => {
            
          }
        }
      ]
    });
    confirm.present();
  }

  alertPresent(title:string, message:string){
    this.alertCtrl.create({
      title: title,
      message: message
    }).present()
  }






}
