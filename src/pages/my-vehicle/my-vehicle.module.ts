import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyVehiclePage } from './my-vehicle';

@NgModule({
  declarations: [
    MyVehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(MyVehiclePage),
  ],
})
export class MyVehiclePageModule {}
