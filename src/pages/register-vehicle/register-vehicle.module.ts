import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterVehiclePage } from './register-vehicle';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    RegisterVehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterVehiclePage),
    BrMaskerModule
  ],
})
export class RegisterVehiclePageModule {}
