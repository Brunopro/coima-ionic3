import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading, MenuController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { PreferencesProvider } from '../../providers/preferences/preferences';

@IonicPage()
@Component({
  selector: 'page-register-vehicle',
  templateUrl: 'register-vehicle.html',
})
export class RegisterVehiclePage {

  load: Loading;

  vehicleForm: FormGroup

  signUpForm: any;



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    private authProv: AuthProvider,
    private preferencesProv: PreferencesProvider,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public menuCtrl: MenuController
    
    ) {


      this.signUpForm = navParams.get('signUpForm')

      console.log('this.signUpForm', this.signUpForm)

      this.vehicleForm = this.formBuilder.group({
        placa: ['', [Validators.required, Validators.minLength(8)]],
        renavan: ['', [Validators.required, Validators.minLength(11)]]
      })
  }


  registerVehicle(){

    this.loadPresent()

    let data_vehicle = {
      placa: this.vehicleForm.controls.placa.value,
      renavan: this.vehicleForm.controls.renavan.value
    }

    //Já tem usuário cadastrado com esse cpf?    
    this.authProv.verifyCpf(this.signUpForm.cpf).then(() => {
      console.log('verify cpf')


          this.authProv.singUp(this.signUpForm).then(() => {

            console.log('0')
    
    
            this.signUpForm.uid = this.authProv.uid
    
            console.log('signUpForm.uid', this.signUpForm.uid)
                  
                  this.authProv.setDataVehicle(this.authProv.uid, data_vehicle).then(() => {
                    let vehicle = [{ placa: data_vehicle.placa, renavan: data_vehicle.renavan}]
                    this.preferencesProv.create('vehicle', vehicle).then(() => {
                      this.preferencesProv.vehicle = vehicle
                      this.preferencesProv.create('user', this.signUpForm).then(() =>{
                          this.preferencesProv.user = this.signUpForm;
                          this.setMenuUser();
                          this.load.dismiss();
                          this.navCtrl.setRoot('HomePage')
                      })
                    }).catch((err) => {
                      this.alertPresent(err, 'err preferences');
                      this.load.dismiss();
                    })
                  }).catch((err) => {
                    this.load.dismiss();
                    this.alertPresent('err setDataVehicle', err)
                  })
                  
    
            }).catch((err) => {
            this.load.dismiss();
            console.log('erro 1', err)
            if (err.code == 'auth/weak-password') {
              this.alertPresent('Erro', 'A sua senha deve ter no minimo 6 caracteres.')
            } else if (err.code == 'auth/email-already-in-use') {
              this.alertPresent('Erro', 'O e-mail digitado já esta em uso.')
            } else if (err.code == 'auth/invalid-email') {
              this.alertPresent('Erro', 'O e-mail digitado não é valido.')
            } else if (err.code == 'auth/operation-not-allowed') {
              this.alertPresent('Erro', 'Não esta habilitado criar usúarios.')
            } else if (err.code == 'auth/network-request-failed') {
              this.alertPresent('Erro', 'Verifique sua conexão')
            } else {
              this.alertPresent('Erro', err)
            }
          })

    }).catch(() => {
      this.load.dismiss()
      console.log('erro 2')
      this.alertPresent('Erro', 'O CPF já esta em uso')
    })

    

    
  }

  setMenuUser() {
    this.menuCtrl.enable(true, 'users');
    this.menuCtrl.enable(false, 'admin');
  }

  loadPresent(){
    this.load = this.loadingCtrl.create({
      content: 'checkando dados, aguarde'
    })

    this.load.present()
  }

  alertPresent(title, message){
    let alert = this.alertCtrl.create({
      title: title,
      message: message
    })

    alert.present()
  }


}
