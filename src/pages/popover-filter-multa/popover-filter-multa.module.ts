import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverFilterMultaPage } from './popover-filter-multa';

@NgModule({
  declarations: [
    PopoverFilterMultaPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverFilterMultaPage),
  ],
})
export class PopoverFilterMultaPageModule {}
