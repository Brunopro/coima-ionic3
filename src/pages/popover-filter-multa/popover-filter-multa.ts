import { Component } from '@angular/core';
import { IonicPage,ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-popover-filter-multa',
  templateUrl: 'popover-filter-multa.html',
})
export class PopoverFilterMultaPage {

  public self:any;

  constructor(
    public viewCtrl: ViewController,
    private navParams: NavParams) {
      this.self = navParams.get('self');
    }

  public select(value):void {
    this.self.changeViewFilterMultas = value;
    this.viewCtrl.dismiss();
  }

}
