import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { VehicleProvider } from '../../providers/vehicle/vehicle';
import { PreferencesProvider } from '../../providers/preferences/preferences';

@IonicPage()
@Component({
  selector: 'page-add-vehicle',
  templateUrl: 'add-vehicle.html',
})
export class AddVehiclePage {

  public registerVehicle : FormGroup;
  load: Loading;
  uid:string

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public vehicleProv: VehicleProvider,
    public loadindCtrl: LoadingController,
    public preferencesProv: PreferencesProvider) {

    this.registerVehicle = this.formBuilder.group({
      placa: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      renavan: ['', Validators.compose([Validators.required, Validators.minLength(11)])],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddVehiclePage');
  }

  saveVehicle(){

    this.loadingPresent()


    let vehicle = {
      placa: this.registerVehicle.controls.placa.value.toUpperCase(),
      renavan: this.registerVehicle.controls.renavan.value,
      uid: '',
    }

    this.vehicleProv.addVehicleInDatabase(vehicle, this).then(data =>  {
      vehicle.uid = this.uid;
      let vehicles = this.preferencesProv.vehicle;
      //adicionando novo vehicle
      vehicles.push(vehicle)
      this.preferencesProv.vehicle = vehicles;
      this.preferencesProv.create('vehicle', vehicles).then(()=>  {
        this.load.dismiss()
        this.navCtrl.setRoot('MyVehiclePage')
      })
      
      console.log('success!', data)
    }).catch(err => {
      this.load.dismiss()
      console.log('error', err)
    })
  }

  loadingPresent(){
    this.load = this.loadindCtrl.create({
      content: 'Salvando dados, aguarde..'
    })

    this.load.present()
  }

}
