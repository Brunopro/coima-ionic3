import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddVehiclePage } from './add-vehicle';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    AddVehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(AddVehiclePage),
    BrMaskerModule
  ],
})
export class AddVehiclePageModule {}
