import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { EnquadramentoProvider } from '../../providers/enquadramento/enquadramento';
import { MultasProvider } from '../../providers/multas/multas';

@IonicPage()
@Component({
  selector: 'page-list-enquadramento',
  templateUrl: 'list-enquadramento.html',
})
export class ListEnquadramentoPage {

  enquadramento_array:any;
  next_page_url:string;
  array_filter: any = false;
  enquadramento_array_filter:any;
  item_selected: any;
  view_loading: boolean = false;
  enquadramento_array_filter_next_page: any;
  body = {
    code: ''
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public multasProv: MultasProvider,
    public viewCtrl: ViewController,
    public http: HttpClient,
    public enquaProv: EnquadramentoProvider) {
      console.log('enquadramento_selected', this.multasProv.enquadramento_selected)
      this.item_selected =  this.multasProv.enquadramento_selected?  this.multasProv.enquadramento_selected: null;
      this.enquadramento_array = this.navParams.get('enquadramento_array');
      this.next_page_url = this.navParams.get('next_page_url');
      console.log(this.next_page_url)

    this.enquadramento_array.forEach(element => {
      if(this.item_selected){
        if(element.id == this.item_selected.id){
          //elemento salvo recebe true
          element.selected = true;
        } else {
          //demais elementos false
          element.selected = false;
        }
      } else {
        //todos os elementos recebem false
        element.selected = false;
      }
    });


    console.log(this.enquadramento_array)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListEnquadramentoPage');
  }

  selectItem(item){
    console.log('item', item)
    this.multasProv.enquadramento_selected = item;

    this.viewCtrl.dismiss({ item });
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    if(this.next_page_url){
      this.http.get(this.next_page_url).subscribe(data => {
        let new_array = data[0]["data"]
        this.next_page_url = data[0]["next_page_url"];
        console.log('next_page_url', this.next_page_url)
  
        new_array.forEach(element => {
          if(this.item_selected){
            if(element.id == this.item_selected.id){
              //elemento salvo recebe true
              element.selected = true;
              this.enquadramento_array.push(element)
            } else {
              //demais elementos false
              element.selected = false;
              this.enquadramento_array.push(element)
            }
          } else {
            //todos os elementos recebem false
            element.selected = false;
            this.enquadramento_array.push(element)
          }
          
        })
        infiniteScroll.complete();
      }, error => {
        console.log('error')
        infiniteScroll.complete();
      })
    } else {
      infiniteScroll.complete();
    }

  }

  getItems(ev: any) {
    // set val to the value of the searchbar
    const val = ev.target.value;

    console.log('val1', val)

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.view_loading = true;
      this.array_filter = 3;
      this.body.code = val;
      this.enquaProv.searchEnquaByCode(val).subscribe(data => {
        console.log('data["data"].length', data["data"].length > 0)
        if(data["data"].length > 0){
          console.log('dados filtrados', data)
          //his.array_filter = true
          this.enquadramento_array_filter = data["data"];
          this.enquadramento_array_filter_next_page = data["next_page_url"];
          console.log('this.enquadramento_array_filter_next_page = data["next_page_url"];', this.enquadramento_array_filter_next_page)
  
          this.enquadramento_array_filter.forEach(element => {
            if(this.item_selected){
              if(element.id == this.item_selected.id){
                //elemento salvo recebe true
                element.selected = true;
              } else {
                //demais elementos false
                element.selected = false;
              }
            } else {
              //todos os elementos recebem false
              element.selected = false;
            }
  
            this.array_filter = true;
            this.view_loading = false
          });
          
        } else {
          this.enquadramento_array_filter = []
          this.array_filter = true;
          this.view_loading = false
        }
      }, err => {
        console.log('err',err)
        this.array_filter = true;
        this.view_loading = false
      })
    } else {
      this.array_filter = false;
    }
  }

  getItemsDoInfinite(infiniteScroll) {

    if(this.enquadramento_array_filter_next_page !== null ){
      this.http.post(this.enquadramento_array_filter_next_page + '&data=' + this.body.code, this.body ).subscribe(data => {
        let new_array = data["data"]
        this.enquadramento_array_filter_next_page = data["next_page_url"];
  
        new_array.forEach(element => {
          if(this.item_selected){
            if(element.id == this.item_selected.id){
              //elemento salvo recebe true
              element.selected = true;
              this.enquadramento_array_filter.push(element)
            } else {
              //demais elementos false
              element.selected = false;
              this.enquadramento_array_filter.push(element)
            }
          } else {
            //todos os elementos recebem false
            element.selected = false;
            this.enquadramento_array_filter.push(element)
          }
        })

        infiniteScroll.complete();
      }, error => {
        console.log('error')
        infiniteScroll.complete();
      })
    } else {
      infiniteScroll.complete();
    }

  }

  onCancel(){
    this.array_filter = false;
  }

  closeModal(){
    this.viewCtrl.dismiss()
  }

}
