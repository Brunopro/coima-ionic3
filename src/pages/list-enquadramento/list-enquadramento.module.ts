import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListEnquadramentoPage } from './list-enquadramento';

@NgModule({
  declarations: [
    ListEnquadramentoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListEnquadramentoPage),
  ],
})
export class ListEnquadramentoPageModule {}
