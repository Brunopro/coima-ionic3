import { Component } from '@angular/core';
import { IonicPage, Platform, AlertController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { Network } from '@ionic-native/network';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  private alert:any = null;

  constructor( 
    private afd: AngularFireDatabase,
    private platform: Platform,
    private preferencesProv: PreferencesProvider,
    private alertCtrl: AlertController,
    private network: Network) {

  }

  ionViewDidLoad(){
    this.platform.ready().then(() => {
      if(this.network.type !== 'none'){
        this.preferencesProv.get('user').then((user) => {
          console.log('preferencesProv', user)
          this.afd.database.ref('users').child(user.uid).once('value', (snapshot) => {
            if(snapshot.exists()){
              console.log('snapshot.val()',snapshot.val(), snapshot.val().admin)
              if(user.admin == 0 && snapshot.val().admin == 1){
                console.log('this.alert', this.alert)
                if(this.alert == null){
                  this.alert = this.alertCtrl.create({title: 'Alerta', message:'Você agora tem privilégios de administrador para poder usar, faça login novamente'}).present();
                }
              }
            } else {
              console.error('erro/datanotfound');
            }
          })
        })
      } else {
        console.log('network', this.network.type);
      }
    });

    /*this.afd.database.ref('prefecture').set({
      name: 'timon',
      description: 'PREFEITURA MUNICIPAL DE TIMON - MA'
    })
    */
  }
  
}
