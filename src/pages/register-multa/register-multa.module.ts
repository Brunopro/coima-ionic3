import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterMultaPage } from './register-multa';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  declarations: [
    RegisterMultaPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterMultaPage),
    BrMaskerModule,
    IonicSelectableModule
  ]
})
export class RegisterMultaPageModule {}
