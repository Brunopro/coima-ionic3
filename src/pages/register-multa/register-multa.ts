import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, AlertController, ToastController, Platform, Loading, ActionSheetController } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { CameraProvider } from '../../providers/camera/camera';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { MultasProvider } from '../../providers/multas/multas';
import * as moment from 'moment';

import { Storage } from '@ionic/storage';
import { EstadoProvider } from '../../providers/estado/estado';
import { IEstado } from '../../interfaces/IEstado';
import { AgenteProvider } from '../../providers/agente/agente';
import { IAgente } from '../../interfaces/IAgente';
import { IMulta } from '../../interfaces/IMulta';
import { Network } from '@ionic-native/network';
import { OverlayProvider } from '../../providers/overlay/overlay';



@IonicPage()
@Component({
  selector: 'page-register-multa',
  templateUrl: 'register-multa.html',
})
export class RegisterMultaPage {

  agents: IAgente;
  private user: any;

  agente_current: IAgente;

  public agent: String;

  public myDate: String;

  btnlocation: boolean;

  enquadramento_array = [];

  uf_emplacamento_array: IEstado[];

  municipio_array = [];
  municipio: any[] = []

  public registerMulta: FormGroup;
  public uf_emplacamento_selected: boolean = false;


  //formulario
  numero_auto_infracao;
  cod_enquadramento;
  placa;
  observacao;
  agentes_id;
  data_infracao;
  hora_infracao;
  uf_orgao_autuador;
  municipio_id;

  local_ocorrencia: String;

  //fim-formulario

  //Usadas para solicitar permissão de localização
  gaveLocationPermission: boolean = false;
  hasEnabledLocation: boolean = false;
  doneChecking: boolean = false;
  andrress: any;
  local_add:boolean = false;


  imagem_add:boolean = false;
  enable_select_municpio_emplacamento:boolean = false;
  lastIndex:number = 0;
  limit:number = 10;
  filtre_leagth:number = 0;
  selected_enquadramento:boolean;
  enquadramento_next_page_url:any;

  load:Loading;

  foto_teste_base64:string = 'iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADeAAAA3gB2VzOMgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABCxSURBVHic7Z15vFVVFce/i/dkUFFEJQU/iiMUFfkJzSGVnCNFTBETP1oqlVNzDpVNH1Pq02RSkZkNmuWAEySZiUOaWUnYHGpmCuIslPaA91j9sc7Fy3l7n3vPvmd4d/h9PvuPe865a++z1u/sce21RVXpwCAiAuwJHACMAUZXpW2Al4HlsfR3YKGqPl9GmRuFtDsBRGQocBBwFHAkZui06APuA24GblHVR7IrYb5oWwKIyEjgU8B7gU0yFr8Y+JSqLsxYbuZoOwKIyDDgA8D5wOY5Z3cXcI6q/i7nfILRVgQQkROBi4HtCs76OuCjqvpEwfnWRFsQQES6gUuB99fx+P+A27Gvd1mUlgNPYU1FpXM4BhgPTAV2q0Pu08DRqnp/yuLnipYngIhsCVwPTE547OXomZuAX6jqKynzGA9MA44Ddk94dDUwS1WvTCM/V6hqyyZgAvAooJ60BvgWsE2GeR4N/C0hTwVmA4PK1o+qti4BsGr5hQQjXAPsklPeXcBpWNPhy39O2TpqWQIAI4B/eBS/GjiloHKMAu5NIMHppeuq7ALkoPQu4BcehT8N7JtS3l7ALcBjwKeBwSn/Pxj4nqc8a4EDOwTIlgCXepT9N2D7AHnx9vz4wHJ93FOuF/JqitqOAMA7PEp+Htg5QN6bHbLmN1C+Szzl+y3RiKzoNIgWgYgMwnrXcfQC01X10QCxw+u8Vi8+gs0xxLEHML0BucFoGQIAJwGvd1z/sKouKrowLqhqHzADeNhx+wsislHBRWoNAkQrep933LpHVecEyhwCHOq4tZOI7B8iE0BVX8Q9I7kLMCtUbjDKbrczavs/irtt3StQ3uHA4x6ZlXQnMKaBMi90yFwBbFKo7so2XkYEcM28zQuUdQjQU8P41QZ7S2A+EzE/grjMGR0CpFPkeIcS+4DdAmRtBayq0/iV9EADZb/KIe+nReqvFfoA0xzX7lPVpQGyziF9L39PEQldXr7CcW2KiAwOlJcarUCAox3XbgyUdYrj2m+wJd+tsKHastj9xar6ZGB+92ATQdUYjrmoFYOyq/AGq/8xwDr6V6NjA2S9xiFHgfGx57YELgT+BcwD3tTgO3zfkedlhemwbCM2qLwjHcpbHChrkkPW0gLeYaoj3weL0mGzNwEuD97fB8p6GFN+NXYVkdcGyqsXrvKGeCYHodkJsK3j2ooQQaq6Evin49aPRGSaiHSFyK0Dz2DNWDVGRVPbuaPZCeD6Up5qQN5tjmuTsE7lCyKySES+JCJvaiCPDaCqvcBzscvdWKczdzQ7AVw1QCMEOB/4t+feZsDbsGXd34nIhRkO11xldr1b5mh2Aox0XIsPq+qGqq4C3oM5aiShG/gkcFtGVbWrzK53yxzNToDMFae2crgn8Kc6Hp8MfLCR/CJkSuQ0aHYC5FJ1quoSrO0/C7gaWEr/EUIFXxCRMQ1mmXVTVjeanQCuHn8mbaeqrlHVb6rqTFUdB+yALTmviT06DNgvNJ9o00q8w+fqGOaCZieA6yvJZQytqk+o6meAGxy3JzUgehT97fCMqsaHhrmg2QngqgGCjSEiE0RktohcJSIu7yKA+Y5r40PzxF3eoLmMEDQ7ARbTv23eXUTGphUUuWMtAM4FZgIPici5jkePclzzDR3rgWsx68EG5KVCUxNAVZcBrq3XLqXWwuHA2Krfg4DZIvJDEdlbRMaKyEUe2X8JyK/S/k913ApdzUyPshd0MlhMOY/+iyn3BMgZjXtlsVb6D7BVYNkPdMhbRcrNJ+28GAS2ozeOfUWkni3b66Gqy4HLA/Kfo6qhPXaX/8GtqhofaeSHsr/gjGqBTHwCAQG+5pDlSwsI/Frp+ARmSoCsvYJnYZ4/PsP3Ad8FhjRQ5o5XcIYEGILbjfvuBmQOxcLJxGU+BkxosLyutl8pYbdwK/QBUNXVwAWOW/uLyNmBMntwLw8/rqpBvX4AEdkC+I7j1lKsVikULUGACFcBDzmuf01EDim6MC5Ew77rsV1AcXxCzTegULQMAdSmTl0TN13AtWlHBRFW1XmtXnwDq/7juF9V5zUgNxxlt9859Ae+grt9fRjYKUDeX2Nyjgss1wWecj0L7Fiavso2WA4E6MLdw1Zshe1tKeVVXML+iXkMbZTy/0OBH3vKswY4oFR9lW2wnEiwOf5IXWuBMwoqx2gs+INvOPm+0nVVdgFyVP6uJEcJu4nYpo8M8+4GzsA8fn35d6KEFUCCCcAjCUboxYZkWcYJPIZXPYh86WIGSJzAdogUOhK4luT9dkVFCu0BTlXVq9PIzxMtTwCAaFPHV7Eo4bWQV6zg5cA0HWCRw9uCABWIyLHAl4AdC8x2HTYKOFdVC3H0TIO2IgBAtJnjdOywiLx33/wcOE9VXTOUAwJtR4AKRGQzzJnkDLI/OOLX2Ikhd2YsN3O0LQEqiHwBJ2OduKOwtj0teoFfYRNGN6tqIz6CQRCREdg086HAvtiU9ZNYH+a3wDXqMHbbE6Aa0alhe2CESHNq2K2qWshOnjiiEHnfwLyLknYwLwE+rqq/3OD/HQI0L0RkJ2z4mjT0jGMBFjm1B1poNbDdICIHY+7jaYwPcAQwt/KjQ4AmhIgciIWwHxEo4uSKo0ynCWgyiMh+2Gqn66zDF7GI5Asxd/UdsHMRXXsZeoE9OgRoIojIXthhGK5YhkuAd6rqY47/TcFqjHgncXanCWgSiMgkbGLJZfwHgcku4wOo6q3YxFccUzo1QBNARCZiwam3cNx+CDt2JnEYKiKbYgdnbBDWplMDDHCIyATgl7iN/xfgkHrmIFT1v1jU0w3QIcAAhoiMA+7AvWbxD+AgVX02hcgh8QsdAgxQiMjOwCIshG0cj2LV/tMp5HUDO8evdweXsGCIyPaYm9cW2Ph3BBa6bSWvTssuA5andegYaIjiG9yJTUHH8S/MsXV5SrGn0b8meWlAdgJFZDgwBTgAeEOU0qzYrQT+iE173qKqf8+8kDkhCj1/D26fhScwL2Jnbz9B5mjgD1g4mmp8f8AQIFqenY5NWhyMo71qAI9goV3mAw/QPzRrnlijdcb7EZFtgbuxmi6Op4D9VfWRNJmLyMYYod4cu/U/YLfSnRKxNu4i4CXq35bdTGkN1mbPx4JQjvDoYRR+V/YVBHgwY9vdr/fIvFi1RK9gLO7+nIiJZRupyPQ8cKhDF3/yPP8s8PpAHV/okfkksLmW5RUsIjOwI163TvnXHiwgUyU9gc2MjalKo8m2+cgD67CZudlYZ/YO3Kt6L2K9/SVpMxCRmdiG2TheAfZT1cVAsTUAFsTxZur7Uv6LhWw5BnPSGJUin7GYB/Bd2KJH2V+9L92Af+fQS8CkQD3vjfvks3XYesGrzxZo/DdiX2wtpfweeB8wPKN8t8aGQAuw6rSn4BQSeGoVsHfg++6AnZLuknt+v+cLMv4h2NDM98IrgW8DuxdFyAKJvynmYubbIBpPL2NVdEhew7Hhr0vuj5z/KUABM7ENmb4Xvh3YumxDFUSGd9WoEV7B2vwQ2YOwkYZL7r144hnl/cIHYsMgV6HWYcGXB8QeuQJJ8E2PPnqAwxqQ64uL8FjSB5bni47HerGuQj0HHF62MUoiwPYenRzTgMzTPDJXUWMImddLbolNfrgK9QCwfdmGKJkErk7auEBZkz21bC8wpdb/81oMugTYyXH9NmCqFhkJc2DCtQqbel+BiOyCHV65keP2x9Q8gZKRA7sPw/3l/5Vo9qmdE7bIE9fN4wFyRmCbUly6/k69cjL1B4gWHuY6bj0PHKF2Nl+74zzHtVTh4aO1/WuBcY7bi4Az65XVUBMgIjtiS7bDo7QPG4Zcr+AsVXUdythWEJH3YG7acaQ97fQSbG4ljoeBYzVNvMHAamwo8BnqW8hZWHKVOxZbYq4Z1Blrm4dmnLYF3o59sS79PAVsmeJ9zvLIeQHYLbV+AhS6D/4evmtiY2yJxr+AVyNyP+tTEOYp+7nIGPW8V5bpiBTvcxjutY21hE4gpVToW7EdJ/W+3DrglJKMvwn9fQy+4nn2hBIMr8DlKd7ntY73qaTgcHN5Gr9UEmC+gy/HyuLsHVP/PH1WqQ/4InWGmyd5XuXrDekpA+M/h/UHPo/fqaEsElxSVYYeYKLnuekFGn8pKVb6sObpbo+sW4GuXAlQw/iLgZFVz26K+Z/5SHBqCSTYBzve1XuuDzaRck5knKyXg5dhizSfxbZmD0tZ/is8+vwzsFnD+snK+FX/2SSBsaWQoFkTdlK5S4/PkFHnOlPjx0hwVwIJTitbuQM9YfEHXWcK9QD7ZpZP1savkrExtrmhQ4L0xp+YoP+TMs0rD+PHSLCoQ4JUxn8N7vOPFLgo8/zyMn6MBHckkGBW2UofKAmbOfyNR1c3EEV0yYUAeRi/SvYwbItzhwR+HW2dUFsuBjbOJd+8jR8jwe0dEjh1swe2z8Glm+XAmBzzZlzexq/KbCgW48ZHgveWbYyCDT8SmxF0+fArtpYStDcgDQHmFGH8GAl+3s4kALYDPk2yq/xqYps48kjd2CKDC/M0h/CnqtojItOwuLqHx24LMFdEUNXLGsknigpedgCMQdie/G2itCc2Gzixxv+WY06i/UK65IGZ+Fn4gRy/giHYXLavJqh7hQvrQE3F1iRuxt+eNkO6lwyPsKlDd3SRvBp2ds4k+FkoCbAv7IPYPsKyDddoWon5L6Q6lq5hG0SK7AKuTijcWTmTYEECCd7v+d/rgPsHgOEaTa9gp5jU7RWUOQGqSPCThIKemSMJBuPf1tSPBMCHsE5S2cYLTX3YoRLnA6PLMHwlbRAfIDpc6cfADNw4U1W/5bnXEKJO2/XAkY7bih32OFdEPou19bVQ2SxRJvowV7SnsSgfK7CZvp+p6jNlFmw9HF9jF3ANKavkDGsCX/yAdfiPhK1UpbOxXvZ2ZX5VzZR8hqj4nfsMkduRp5hzxo0Jhnalu4Bdy1ZmM6YkQ3QD1yWQILcJm4gEN9Rp/DnksEjSLqmWIWqRILf5+4gE82oY/6tlK7DZUz2G6MYfaizXNf0aef+wbOW1Qqo5Vaq2zej4qEqOQ4DLROTUWnJCUJX3Hx23V+eRZ7uhrrnyKkPc6LgtwHdF5JQsCxbL+1LHrQl55NduqHuxRFXXYvMDNzluV0jw7ozKFcd/HNfaPcZAJki1WhaR4DhsrO6S9T0ROTmLglUQHeZ4tuPWiizzaVekXi6NSDAdO4TIJe8KETmp0YLBeuPPxY5CjcNVE3WQEsGhYmtM3a4D3q2qVwYXTGQQcBng6mCuwDZGdDqCDSLYYUItzs+x2EqeS+4PROTEENmR8a/AbXwFTu4YPyNkMFYfjH85tw+YmVJeF3ClR54Cnyh77NxKKRshySToBU5IYfwkv4SO8QciASLjJXn39ALvqvH/bvyrkAqcU7ayWjFlKyzZz68XON7zv1rz/h8uW1GtmrIXaCTwrdv3AjNizw/GhnQ+4+fmk9hJORCgigQ+3/+1wPQq4ye5gp1etoJaPeV2ZIyIDMVmDA913O7FDlA6AQuhFodi/gaX51K4DtYj1zODIhLcgjuooQ+VKCI/yKVQHWyA3A+NSkmCPmwG0XXYUQc5oJBTw0RkGEaCgxMe6wNOVNWf5l6gDtajsGPjIhLMBw5y3K5MFl1XSGE6WI9Czw2MSLAAO0qmgrXY0NDlbNJBzij84MgopPyHgFnAEuDLqnpfoYXoYD3+Dyo1u5QOLQ32AAAAAElFTkSuQmCC'

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public diagnostic: Diagnostic,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public cameraProv: CameraProvider,
    public http: HttpClient,
    private formBuilder: FormBuilder,
    public plt: Platform,
    public preferencesProv: PreferencesProvider,
    public multaProv: MultasProvider,
    public storage: Storage,
    public estadoProv: EstadoProvider,
    public agenteProv: AgenteProvider,
    private network: Network,
    private overlayProv: OverlayProvider,
    private actionSheetCtrl: ActionSheetController) {

    this.preferencesProv.get('user').then((user) => {
      this.user = user;
    });

    //Setando hora e dia atual
    let date = moment().format('YYYY-MM-DD');
    console.log('date', date)
    let hora = moment().format('HH:mm:ss');
    console.log('hora', hora);

    this.registerMulta = this.formBuilder.group({
      numero_auto_infracao: ['', Validators.required],
      enquadramento_id: ['', Validators.required],
      local_ocorrencia: ['', Validators.required],
      placa: ['', Validators.compose([Validators.required])],
      observacao: [''],
      agentes_id: ['', Validators.required],
      data_infracao: [date, Validators.required],
      hora_infracao: [hora, Validators.required],
      uf_emplacamento: [null],
      emplacamento_municipio_id: [''],
      municipio_emplacamento: [null,],
      foto_infracao: [null],
    });

    console.log('estado exite?')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterMultaPage');
    this.getDataSqlite()
  }

  ionViewCanLeave() {
    //this.clearForm();
  }

  async getDataSqlite() {
    let load = this.loadingCtrl.create({
      content: "Atualizando dados"
    });
    load.present();

    this.plt.ready().then(() => {
      this.estadoProv.getAll().then((res: IEstado[]) => {
        console.log('estadoProv.getAll()', res)
        this.uf_emplacamento_array = res;
        this.preferencesProv.get('user').then((user) => {
          console.log('user cpf para pesquisar agente', user)
          this.agenteProv.getAgenteByCpf(user.cpf.replace(".", "").replace(".", "").replace("-", ""))//converter aqui user.cpf
            .then((res: IAgente) => {
              
              this.agente_current = res;
              this.registerMulta.controls.agentes_id.setValue(this.agente_current.id);
              console.log('this.agente_current', this.agente_current)
              load.dismiss();
            })
        })
      }).catch(() => {
        console.log('error estadoProv getAll')
        load.dismiss();
        this.navCtrl.setRoot('HomePage')
        this.overlayProv.alertPresent({title:'Erro', message:'Ocorreu um erro ao carregar dados, tente novamente!'})
      })
    });
  }

  submit() {
    console.log('submit', this.registerMulta.value, this.registerMulta.controls.numero_auto_infracao.value.length)

    this.loadingPresent()

    if (this.registerMulta.valid) {


      if (this.registerMulta.controls.numero_auto_infracao.value.length < 10) {
        this.load.dismiss();
        return this.alertPresent('Alerta', 'numero auto infracao deve ter 10 digitos')
      } else if (this.registerMulta.controls.uf_emplacamento.value != '' && this.registerMulta.controls.municipio_emplacamento.value == '') {
        this.load.dismiss();
        return this.alertPresent('Alerta', 'ao adicionar estado emplacamento é obrigatório adicionar município emplacamento')
      } else if (this.registerMulta.controls.placa.value.length < 8) {
        this.load.dismiss();
        return this.alertPresent('Alerta', 'Placa inválida')
      } else if (this.registerMulta.controls.placa.value.length == 8) {
        this.registerMulta.controls.placa.setValue(this.registerMulta.controls.placa.value.replace(/^[0-9a-zA-Z]+$/, ''));
        if (this.registerMulta.controls.placa.value.length < 8) {
          this.load.dismiss();
          return this.alertPresent('Alerta', 'Placa inválida')
        } else {
          this. sendMultaApi();
        }
      } else {
        this.sendMultaApi()
      }


    } else {
      console.log("campos invalidos")
      this.load.dismiss()
      this.alertPresent('Alerta', 'verifique se todos os campos estão preenchidos corretamente')
    }


  }

  sendMultaApi(){
    
    let multa: IMulta = {
      id: null,
      numero_auto_infracao: this.registerMulta.controls.numero_auto_infracao.value.toUpperCase(),
      enquadramento_id: this.registerMulta.controls.enquadramento_id.value,
      placa: this.registerMulta.controls.placa.value.toUpperCase(),
      observacao: this.registerMulta.controls.observacao.value,
      agentes_id: this.registerMulta.controls.agentes_id.value,
      data_infracao: this.registerMulta.controls.data_infracao.value,
      hora_infracao: this.registerMulta.controls.hora_infracao.value,
      local_ocorrencia: this.registerMulta.controls.local_ocorrencia.value,
      foto_infracao: this.registerMulta.controls.foto_infracao.value,
      municipio_emplacamento_id: this.registerMulta.controls.municipio_emplacamento.value,
      uf_emplacamento_id: this.registerMulta.controls.uf_emplacamento.value ? this.registerMulta.controls.uf_emplacamento.value["id"] : null,
      status: 1,
      enquadramento_codigo: this.multaProv.enquadramento_selected.codigo,
      agente_nome: this.agente_current.nome,
      enquadramento_descricao: this.multaProv.enquadramento_selected.descricao,
      enquadramento_desdobramento: this.multaProv.enquadramento_selected.desdobramento,
      estado_id: 48,
      municipio_emplacamento_nome: this.multaProv.municipio_selected.nome,
      uf_emplacamento: this.registerMulta.controls.uf_emplacamento.value ? this.registerMulta.controls.uf_emplacamento.value["uf"] : null,
    }


    let multas: IMulta[] = [];

    console.log('array multas', multa, 'form');

    if (this.network.type != 'none') {
      multas.push(multa);
      //mandar user
      console.log('this.user register multa', this.user)
      let user = {
        cpf: this.user.cpf.replace(".", "").replace(".", "").replace("-", ""),
        company: this.user.company
      }
      this.multaProv.saveMultaInApi(multas, user).subscribe((data) => {
        this.load.dismiss();
        if (data) {
          console.log("dados salvos com sucesso", data);
          let multas_existentes = [];
          multas_existentes = data["multas_existentes"];
          let multas_salvas = [];
          multas_salvas = data["multas_salvas"];
          if (multas_existentes.length > 0) {
            this.alertPresent('Erro', 'Já existe multa com o número auto infração igual á ' + data["multas_existentes"][0]["numero_auto_infracao"])
          } else if (multas_salvas.length > 0) {
            this.clearForm();
            this.navCtrl.setRoot('ListCreatedMultasPage');
          }

        } else {
          console.log("Else ocorreu um erro ao salvar")
          this.alertPresent('Erro', 'ocorreu um erro ao salvar')
          this.load.dismiss();
        }
      }, err => {
        this.load.dismiss();
        console.log(err)
        if (err["error"]["line"] == 664) {
          this.alertPresent('Erro', 'Codigo enquadramento não consta na nossa base de dados');

          console.log(err["error"]["message"])
        } else {
          this.alertPresent('Erro', 'ocorreu um erro ao salvar')
        }
      });

    } else {
      multa.enviado = 0;
      this.createMultaSql(multa).then(() => {
        this.load.dismiss();
        this.clearForm();
        this.navCtrl.setRoot('ListCreatedMultasPage');
        this.toastPresent('Dados da multa salvos offline')
      })
    }
  }


  modalMaps() {


    if (this.network.type == 'none') {
      this.overlayProv.alertPresent({ title: 'Alerta', message: 'Verifique sua conexão com a internet para continuar' });
    } else {
      this.diagnostic.isLocationAuthorized().then((resp) => {
        console.log('isLocationAuthorized()?', resp)
        if (resp == true) {
          this.diagnostic.isLocationAvailable().then((res) => {
            console.log('location esta ligado?', res)
            if (res == true) {
              let myCallbackFunction = (_params) => {
                return new Promise((resolve, reject) => {
                  this.andrress = _params;
                  console.log('this.andrress', this.andrress)
                  this.registerMulta.controls.local_ocorrencia.setValue(this.andrress);
                  this.local_add = true;
                  resolve();
                });

              }
              this.navCtrl.push('GpsPage', { callback: myCallbackFunction })

            } else {
              this.alertPresent('Erro', 'Por favor, verifique se sua localização está ativa')
            }
          })
        } else {
          this.requestPermission()
        }
      }).catch((err) => {
        console.log('isLocationAuthorized().catch()?', err)
      })
    }

  }


  modalEnquadramento() {
    let selectModal = this.modalCtrl.create('ListEnquadramentoTestePage', { enquadramento_array: this.enquadramento_array, next_page_url: this.enquadramento_next_page_url });
    selectModal.present();

    selectModal.onDidDismiss(data => {
      console.log('data', data)
      if (data) {
        console.log(data["item"]["id"]);
        this.selected_enquadramento = true;
        this.registerMulta.controls.enquadramento_id.setValue(data["item"]["id"])
      } else {
        console.log('nada a fazer')
      }
    });
  }

  async openActionSheetMultaImagem() {

    const actionsheet = await this.actionSheetCtrl.create({
      title: "Opções",
      buttons: [{
        text: 'Camera',
        icon: 'camera',
        cssClass: 'EditionIcon',
        handler: () => {
          console.log('tirar foto')
          
          let load = this.overlayProv.loadPresent();
          this.cameraProv.takePictureCamera(load).then((img:string) => {
            this.registerMulta.controls.foto_infracao.setValue(img)
          }).catch((err) => {
            console.log('return error imagem', err)
          });
        }
      },
      {
        text: 'Galeria',
        icon: 'images',
        cssClass: 'EditionIcon',
        handler: () => {
          console.log('galeria')
          let load = this.overlayProv.loadPresent();
          this.cameraProv.takePictureGalery(load).then((img:string) => {
            this.registerMulta.controls.foto_infracao.setValue(img)
          }).catch((err) => {
            //this.load.dismiss()
            console.log('deu erro', err)
            if (err == 20) {
              this.alertPresent("Erro", "Conceda permissão de acesso sua camera para continuar")
            } else if (err == "Sem imagem selecionada") {
              console.log("nenhuma imagem selecionada")
            } else {
              this.alertPresent("Erro", "Ocorreu um erro, tente novamente")
            }
          })
        }
      }]
    });
    await actionsheet.present();
  }

  removeLocate() {
    this.local_ocorrencia = null;
    this.local_add = false;

    this.toastCtrl.create({
      showCloseButton: true,
      message: 'Endereço excluido!',
      position: 'bottom',
      closeButtonText: 'Ok',
      duration: 3000
    }).present()
  }


  removeImagem() {
    this.registerMulta.controls.foto_infracao.setValue('');
    this.imagem_add = false;

    this.toastCtrl.create({
      showCloseButton: true,
      message: 'Imagem excluida!',
      position: 'bottom',
      closeButtonText: 'Ok',
      duration: 3000
    }).present()
  }

  requestPermission() {

    let authorizationLoader = this.loadingCtrl.create({
      content: "Checando permissões..."
    });


    this.diagnostic.isLocationAuthorized().then((resp) => {
      authorizationLoader.dismiss().then(() => {
        this.gaveLocationPermission = resp;
        if (this.gaveLocationPermission) {
          let enablingLoader = this.loadingCtrl.create({
            content: "Checando status do GPS..."
          });
          enablingLoader.present();
          this.diagnostic.isLocationEnabled().then((resp) => {
            this.doneChecking = true;
            this.hasEnabledLocation = resp;
            enablingLoader.dismiss();
            if (this.hasEnabledLocation) {
              this.modalMaps()
            } else {
              this.alertPresent('Falha ao salvar localização', 'Por favor verifique se o seu gps esta ativo')
            }
          }).catch((err) => {
            enablingLoader.dismiss();
            this.alertPresent('Falha ao salvar localização', 'Por favor verifique se o seu gps esta ativo')
          })
        } else {
          this.diagnostic.requestLocationAuthorization().then((res) => {
            if (res != 'denied') {
              this.diagnostic.isLocationAuthorized().then((res) => {
                this.gaveLocationPermission = res;
                if (this.gaveLocationPermission) {
                  let enablingLoader = this.loadingCtrl.create({
                    content: "Checando status do GPS..."
                  });
                  enablingLoader.present();
                  this.diagnostic.isLocationEnabled().then((resp) => {

                    this.doneChecking = true;
                    this.hasEnabledLocation = resp;
                    enablingLoader.dismiss();
                    if (this.hasEnabledLocation) {
                      this.modalMaps()
                    } else {
                      this.alertPresent('Falha ao salvar localização', 'Por favor verifique se o seu gps esta ativo')
                    }
                  }).catch((e) => {
                    console.log(e);
                    enablingLoader.dismiss();
                  })
                }
              })
            } else { // denied location
              this.alertPresent('Ocorreu um erro', 'Por favor autorize a permissão de localização para continuar o cadastro')
            }
          })

          this.doneChecking = true;
        }
      });
    }, err => {
      authorizationLoader.dismiss()
      this.alertPresent('Ocorreu um erro', 'falha ao tentar salvar localização')
    });
  }

  //abrir modal para selecionar municipio
  modalMunicipio() {
    let selectModal = this.modalCtrl.create('ListMunicipioTestePage', { uf_emplacamento: this.registerMulta.controls.uf_emplacamento.value["id"] });
    selectModal.present();

    selectModal.onDidDismiss(data => {
      console.log('data', data)

      if (data) {
        console.log(data["item"]["id"]);
        //this.selected_enquadramento = true;
        this.registerMulta.controls.municipio_emplacamento.setValue(data["item"]["id"])
        console.log('this.registerMulta.controls.municipio_emplacamento.setValue', this.registerMulta.controls.municipio_emplacamento.value)
      } else {
        console.log('nada a fazer')
      }

    });
  }

  changeEstate() {
    this.uf_emplacamento_selected = true;
    this.registerMulta.controls.municipio_emplacamento.setValue('');
    this.multaProv.municipio_selected = {
      id: '',
      nome: '',
      codigo: ''
    }

  }

  clearForm() {
    this.registerMulta.controls.numero_auto_infracao.setValue('');
    this.registerMulta.controls.enquadramento_id.setValue('');
    this.registerMulta.controls.placa.setValue('');
    this.registerMulta.controls.observacao.setValue('');
    this.registerMulta.controls.agentes_id.setValue('');
    this.registerMulta.controls.data_infracao.setValue('');
    this.registerMulta.controls.hora_infracao.setValue('');
    this.registerMulta.controls.local_ocorrencia.setValue('');
    this.registerMulta.controls.municipio_emplacamento.setValue('');
    this.registerMulta.controls.uf_emplacamento.setValue('');
    this.registerMulta.controls.foto_infracao.setValue('');
    this.enable_select_municpio_emplacamento = false;
    this.imagem_add = false;
    this.selected_enquadramento = false;
    this.multaProv.enquadramento_selected = {
      id: '',
      codigo: '',
      descricao: '',
      desdobramento: ''
    }
    this.multaProv.municipio_selected = {
      nome: '',
      id: '',
      codigo: ''
    }
  }


  loadingPresent() {
    this.load = this.loadingCtrl.create({
      content: 'Aguarde, salvando dados'
    })

    this.load.present()
  }


  alertPresent(title, message) {
    this.alertCtrl.create({
      title: title,
      message: message
    }).present()
  }

  toastPresent(message) {
    this.toastCtrl.create({
      message: message,
      duration: 3000,
      showCloseButton: true,
      closeButtonText: 'OK'
    }).present()
  }

  createMultaSql(multa) {
    return this.multaProv.create(multa);
  }



}
