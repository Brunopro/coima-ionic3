import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateMultaPage } from './update-multa';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  declarations: [
    UpdateMultaPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateMultaPage),
    BrMaskerModule,
    IonicSelectableModule
    
  ],
})
export class UpdateMultaPageModule {}
