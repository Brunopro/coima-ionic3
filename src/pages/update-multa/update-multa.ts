import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, AlertController, ToastController, Platform, Loading, ActionSheetController } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { CameraProvider } from '../../providers/camera/camera';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { MultasProvider } from '../../providers/multas/multas';
import { OverlayProvider } from '../../providers/overlay/overlay';
import { Network } from '@ionic-native/network';
import { IAgente } from '../../interfaces/IAgente';
import { AgenteProvider } from '../../providers/agente/agente';
import { environment } from '../../environments/environments';

@IonicPage()
@Component({
  selector: 'page-update-multa',
  templateUrl: 'update-multa.html',
})
export class UpdateMultaPage {

  public agents = [];

  public agent: String;

  public myDate: String;
  private user:any;

  btnlocation:boolean;

  enquadramento_array = [];

  uf_emplacamento_array = [];

  municipio_array = [];
  municipio: any[] = [];

  public registerMulta : FormGroup;


  //formulario
  numero_auto_infracao;
  cod_enquadramento;
  placa;
  observacao;
  agentes_id;
  data_infracao;
  hora_infracao;
  uf_orgao_autuador;
  municipio_id;

  local_ocorrencia: String;

  //fim-formulario
  private url:string = environment.url;
  //Usadas para solicitar permissão de localização
  gaveLocationPermission: boolean = false;
  hasEnabledLocation: boolean = false;
  doneChecking: boolean = false;
  andrress: any;
  local_add = false; 


  imagem_add = false;
  enable_select_municpio_emplacamento: boolean = false;
  lastIndex: number = 0;
  limit: number = 10;
  filtre_leagth: number = 0;
  selected_enquadramento: boolean = true;
  enquadramento_next_page_url: any;

  foto_infracao: string;

  load: Loading;
  multa_id: any;
  multa_current: any;
  
  public agente_current:any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public diagnostic: Diagnostic,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public cameraProv: CameraProvider,
    public http: HttpClient,
    private formBuilder: FormBuilder,
    public plt: Platform,
    public preferencesProv: PreferencesProvider,
    public multaProv: MultasProvider,
    private overlayProv: OverlayProvider,
    private network: Network,
    private agenteProv: AgenteProvider,
    private actionSheetCtrl: ActionSheetController
  ) {

      this.plt.ready().then(() => {
        this.preferencesProv.get('user').then((data) =>  {
          console.log('user preferences', data)
          this.user = data;

          let load = loadingCtrl.create({
            content: "Atualizando dados"
          })
    
          load.present();

          let user = {
            cpf: this.user.cpf.replace(".", "").replace(".", "").replace("-", ""),
            company: this.user.company
          }

          console.log('user preferences', user)
    
          this.agenteProv.getAgenteByCpf(user.cpf)//Mudar aqui user.cpf
          .then((agente: IAgente) => {
            this.agente_current = agente;
            console.log('this.agente_current', this.agente_current)
            load.dismiss();
          }).catch(() => {
            this.navCtrl.setRoot('HomePage');
            this.alertPresent('Erro', 'ocorreu um erro ao carregar dados')
            load.dismiss();
          })
        })
     

      this.multa_current = this.navParams.get('multa');
      this.multaProv.enquadramento_selected.codigo = this.multa_current["enquadramento_dados"]["codigo"];
      this.multaProv.enquadramento_selected.id = this.multa_current["enquadramento_id"];
      this.multaProv.municipio_selected.nome = this.multa_current["municipio_emplacamento_nome"];
      this.foto_infracao = this.multa_current["foto_infracao"] ? this.multa_current["foto_infracao"] : 'https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png';
      this.multa_id = this.multa_current["id"];
      let data_infracao_formater = this.multa_current["data_infracao"].split(' ', 1)

      this.registerMulta = this.formBuilder.group({
        numero_auto_infracao: [this.multa_current["numero_auto_infracao"]],
        enquadramento_id: [this.multa_current["enquadramento_id"]],
        local_ocorrencia: [this.multa_current["local_ocorrencia"]],
        placa: [this.multa_current["placa"]],
        observacao: [this.multa_current["observacao"]],
        data_infracao:[data_infracao_formater[0]],
        hora_infracao: [this.multa_current["hora_infracao"]],
        uf_emplacamento: [this.multa_current["uf_emplacamento"] ],
        municipio_emplacamento: [this.multa_current["municipio_emplacamento"]],
        foto_infracao: [null]
      });

      if (this.plt.is('ios')) {
        console.log('é ios')
        //não exibir buutton de geolocation se for ios
        this.btnlocation = false;
      } else {
        console.log('não é ios')
        //exibir button de geolocation
        this.btnlocation = true;
      }
      
      
      this.http.get(this.url + '/enquadramento').subscribe((data) => {
        console.log('enquadramentos', data)
        console.log(data[0]["data"])
        this.enquadramento_next_page_url = data[0]["next_page_url"];
        this.enquadramento_array = data[0]["data"];
      }, err => {
        console.log('getnameagentes err',err)
        this.navCtrl.setRoot('HomePage');
        this.alertPresent('Erro', 'ocorreu ao tentar recuperar dados com servidor')
      });

      
      this.http.get(this.url + '/estados').subscribe((data) => {
        console.log(data)
        console.log(data[0])
        this.uf_emplacamento_array = data[0];
      }, err => {
        console.log('getnameagentes err',err)
        this.navCtrl.setRoot('HomePage');
        this.alertPresent('Erro', 'ocorreu ao tentar recuperar dados com servidor')
      });





    });
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterMultaPage');
    this.plt.ready().then(() => {
      

    })
  }

  ionViewCanLeave(){
    this.clearForm();
  }

  updateMulta() {
    console.log('form update',this.registerMulta.value )
    this.loadingPresent()
        
    let multa_updated = {
      id: this.multa_id,
      numero_auto_infracao: this.registerMulta.controls.numero_auto_infracao.value.toUpperCase(),
      enquadramento_id: this.registerMulta.controls.enquadramento_id.value,
      placa: this.registerMulta.controls.placa.value.toUpperCase(),
      observacao: this.registerMulta.controls.observacao.value,
      data_infracao: this.registerMulta.controls.data_infracao.value,
      hora_infracao: this.registerMulta.controls.hora_infracao.value,
      local_ocorrencia: this.registerMulta.controls.local_ocorrencia.value,
      foto_infracao: this.registerMulta.controls.foto_infracao.value? this.registerMulta.controls.foto_infracao.value : null ,
      municipio_emplacamento: this.registerMulta.controls.municipio_emplacamento.value
    }



    if(
      this.multa_current["numero_auto_infracao"] == multa_updated["numero_auto_infracao"] &&
      this.multa_current["enquadramento_id"] == multa_updated["enquadramento_id"] &&
      this.multa_current["placa"] == multa_updated["placa"] &&
      this.multa_current["local_ocorrencia"] == multa_updated["local_ocorrencia"] &&
      this.multa_current["observacao"] == multa_updated["observacao"] &&
      this.multa_current["data_infracao"] == multa_updated["data_infracao"] &&
      this.multa_current["hora_infracao"] == multa_updated["hora_infracao"] &&
      this.multa_current["municipio_emplacamento"] == multa_updated["municipio_emplacamento"] &&
      multa_updated["foto_infracao"] == null
    ){
      this.load.dismiss();
      this.navCtrl.pop();
    } else {
      this.verifyFormValidate(multa_updated).then(() => {
        //Formularios validado
        let user = {
          cpf: this.user.cpf.replace(".", "").replace(".", "").replace("-", ""),
          company: this.user.company
        }

        this.multaProv.updateMulta(multa_updated, user).subscribe((data) => {
          console.log('res update', data)
          this.load.dismiss();
          if(data["message"] == "duplicate numero_auto_infracao"){
            this.overlayProv.alertPresent({title:"Falha ao atualizar",message: "Numero auto infracao já está cadastrado"})
          } else {
            this.clearForm();
            this.navCtrl.setRoot('ListCreatedMultasPage');
          }
  
        }, err => {
          this.load.dismiss();
          console.log(err);
          if(this.multaProv.connection){
            this.overlayProv.alertPresent({message: 'Ocorreu um erro ao atualizar multa'})
          } else {
            this.overlayProv.alertPresent({message: 'Verifique sua conexão com a internet'})
            this.navCtrl.setRoot('ListCreatedMultasPage');
          }
        });
      }).catch(() => {
        this.load.dismiss();
      });
    }
  }

  verifyFormValidate(multa_updated){
    return new Promise((resolve,reject) => {
      console.log('')
      if(multa_updated.numero_auto_infracao.length < 10){
        this.overlayProv.alertPresent({title:'Alerta', message:'numero auto infracao deve ter 10 digitos'});
        reject(null);
      } else if(multa_updated.placa.length < 8){
        this.overlayProv.alertPresent({title:'Alerta', message:'Placa inválida'});
        reject(null);
      } else if(multa_updated.placa.length == 8){
        multa_updated.placa = multa_updated.placa.replace(/^[0-9a-zA-Z]+$/, '');
        if (multa_updated.placa.length < 8) {
          this.overlayProv.alertPresent({title:'Alerta', message:'Placa inválida'});
        } else {
         resolve(null);
        }
      } else {
        resolve(null);
      }
    });
  }


  modalMaps() {
    if(this.network.type == 'none'){
      return this.overlayProv.alertPresent({title:'Alerta',message:'Verifique sua conexão com a internet para continuar'});
    } else {
      this.diagnostic.isLocationAuthorized().then((resp) => {
        console.log('isLocationAuthorized()?', resp)
        if ( resp == true) {
          this.diagnostic.isLocationAvailable().then((res) => {
            console.log('location esta ligado?', res)
            if(res == true) {
              let myCallbackFunction = (_params) => {
                return new Promise((resolve, reject) => {
                  this.andrress = _params;
                  console.log('this.andrress', this.andrress)
                  this.registerMulta.controls.local_ocorrencia.setValue(this.andrress);
                  this.local_add = true;
                  resolve();
                });

              }
              this.navCtrl.push('GpsPage', {callback: myCallbackFunction})

            } else {
              this.alertPresent('Erro', 'Por favor, verifique se sua localização está ativa')
            }
          })
        } else {
          this.requestPermission()
        }
      }).catch((err) => {
        console.log('isLocationAuthorized().catch()?', err)
      })
    }
  }


  modalEnquadramento(){
      let selectModal = this.modalCtrl.create('ListEnquadramentoPage', {enquadramento_array: this.enquadramento_array, next_page_url: this.enquadramento_next_page_url });
      selectModal.present();
  
      selectModal.onDidDismiss(data => {
        console.log('data', data)
        if(data){
          console.log(data["item"]["id"]);
          this.selected_enquadramento = true;
          this.registerMulta.controls.enquadramento_id.setValue(data["item"]["id"])
        } else {
          console.log('nada a fazer')
        }
      });
  }

  async openActionSheetMultaImagem() {

    const actionsheet = await this.actionSheetCtrl.create({
      title: "Opções",
      buttons: [{
        text: 'Camera',
        icon: 'camera',
        cssClass: 'EditionIcon',
        handler: () => {
          console.log('tirar foto')
          
          let load = this.overlayProv.loadPresent();
          this.cameraProv.takePictureCamera(load).then((img:string) => {
            this.registerMulta.controls.foto_infracao.setValue(img)
          }).catch((err) => {
            console.log('return error imagem', err)
          });
        }
      },
      {
        text: 'Galeria',
        icon: 'images',
        cssClass: 'EditionIcon',
        handler: () => {
          console.log('galeria')
          let load = this.overlayProv.loadPresent();
          this.cameraProv.takePictureGalery(load).then((img:string) => {
            this.registerMulta.controls.foto_infracao.setValue(img)
          }).catch((err) => {
            //this.load.dismiss()
            console.log('deu erro', err)
            if (err == 20) {
              this.alertPresent("Erro", "Conceda permissão de acesso sua camera para continuar")
            } else if (err == "Sem imagem selecionada") {
              console.log("nenhuma imagem selecionada")
            } else {
              this.alertPresent("Erro", "Ocorreu um erro, tente novamente")
            }
          })
        }
      }]
    });
    await actionsheet.present();
  }

  removeLocate(){
    this.local_ocorrencia = null;
    this.local_add = false;

    this.toastCtrl.create({
      showCloseButton: true,
      message: 'Endereço excluido!',
      position: 'bottom',
      closeButtonText: 'Ok',
      duration: 3000
    }).present()
  }

  
  removeImagem(){
    this.registerMulta.controls.foto_infracao.setValue('');
    this.imagem_add = false;

    this.toastCtrl.create({
      showCloseButton: true,
      message: 'Imagem excluida!',
      position: 'bottom',
      closeButtonText: 'Ok',
      duration: 3000
    }).present()
  }

  requestPermission() {

    let authorizationLoader = this.loadingCtrl.create({
      content: "Checando permissões..."
    });


    this.diagnostic.isLocationAuthorized().then((resp) => {
      authorizationLoader.dismiss().then(() => {
        this.gaveLocationPermission = resp;
        if (this.gaveLocationPermission) {
          let enablingLoader = this.loadingCtrl.create({
            content: "Checando status do GPS..."
          });
          enablingLoader.present();
          this.diagnostic.isLocationEnabled().then((resp) => {
            this.doneChecking = true;
            this.hasEnabledLocation = resp;
            if (this.hasEnabledLocation) {
              this.modalMaps()
            } else {
              enablingLoader.dismiss()
              this.alertPresent('Falha ao salvar localização', 'Por favor verifique se o seu gps esta ativo')
            }
          }).catch((err) => {
            enablingLoader.dismiss();
            this.alertPresent('Falha ao salvar localização', 'Por favor verifique se o seu gps esta ativo')
          })
        } else {
          this.diagnostic.requestLocationAuthorization().then((res) => {
            if (res != 'denied') {
              this.diagnostic.isLocationAuthorized().then((res) => {
                this.gaveLocationPermission = res;
                if (this.gaveLocationPermission) {
                  let enablingLoader = this.loadingCtrl.create({
                    content: "Checando status do GPS..."
                  });
                  enablingLoader.present();
                  this.diagnostic.isLocationEnabled().then((resp) => {

                    this.doneChecking = true;
                    this.hasEnabledLocation = resp;
                    if (this.hasEnabledLocation) {
                      this.modalMaps()
                    } else {
                      enablingLoader.dismiss();
                      this.alertPresent('Falha ao salvar localização', 'Por favor verifique se o seu gps esta ativo')
                    }
                  });
                }
              })
            } else { // denied location
              this.alertPresent('Ocorreu um erro', 'Por favor autorize a permissão de localização para continuar o cadastro')
            }
          })

          this.doneChecking = true;
        }
      });
    }, err => {
      authorizationLoader.dismiss()
      this.alertPresent('Ocorreu um erro', 'falha ao tentar salvar localização')
    });
  }

  //abrir modal para selecionar municipio
  modalMunicipio(){
    let selectModal = this.modalCtrl.create('ListMunicipioPage', {uf_emplacamento: this.registerMulta.controls.uf_emplacamento.value["id"]});
      selectModal.present();
  
      selectModal.onDidDismiss(data => {
        console.log('data', data)
        
        if(data){
          console.log(data["item"]["id"]);
          //this.selected_enquadramento = true;
          this.registerMulta.controls.municipio_emplacamento.setValue(data["item"]["id"])
          console.log('this.registerMulta.controls.municipio_emplacamento.setValue', this.registerMulta.controls.municipio_emplacamento.value)
        } else {
          console.log('nada a fazer')
        }
        
      });
  }

  openModalViewPhoto(){
    let modalPhoto = this.modalCtrl.create('ViewPhotoInfractionPage', {foto_infracao: this.multa_current["foto_infracao"]})

    modalPhoto.present();
  }

  changeEstate(){
    this.registerMulta.controls.municipio_emplacamento.setValue('')
  }

  clearForm(){
    this.multaProv.enquadramento_selected = {
      id: '',
      codigo: '',
      descricao: '',
      desdobramento: ''
    }
    this.multaProv.municipio_selected = {
      nome: '',
      id: '',
      codigo: ''
    }
  }

  
  loadingPresent(){
    this.load = this.loadingCtrl.create({
      content: 'Aguarde, salvando dados'
    })

    this.load.present()
  }


  alertPresent(title, message) {
    this.alertCtrl.create({
      title: title,
      message: message
    }).present()
  }


}
