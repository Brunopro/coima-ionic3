import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavParams, Platform, Loading, Checkbox, ViewController } from 'ionic-angular';
import { MunicipioProvider } from '../../providers/municipio/municipio';
import { IMunicipio } from '../../interfaces/IMunicipio';
import { SQLite } from '@ionic-native/sqlite';
import { MultasProvider } from '../../providers/multas/multas';

@IonicPage()
@Component({
  selector: 'page-list-municipio-teste',
  templateUrl: 'list-municipio-teste.html',
})
export class ListMunicipioTestePage {

  @ViewChild('checkbox') mapElement: ElementRef;
  checkbox: Checkbox;

  public municipios:IMunicipio[];
  municipio_array = [];
  next_page_url:string;
  array_filter: any = false;
  municipio_array_filter = [];
  item_selected: any;
  view_loading: boolean = false;
  municipio_array_filter_next_page: any;
  body = {
    uf_emplacamento: ''
  };

  body_filter = {
    nome: '',
    uf_emplacamento: ''
  }

  uf_emplacamento: any;
  load: Loading;
  next_page: any;
  total_page: any;
  current_checkbox: Checkbox;
  municipio_array_filter_paginate: any;
  municipio_array_filter_total_page: any;


  constructor(
    public navParams: NavParams,
    public muniProv: MunicipioProvider,
    public platform: Platform,
    public sqlite: SQLite,
    public multasProv: MultasProvider,
    public viewCtrl: ViewController
    ) {
      this.item_selected =  this.multasProv.municipio_selected?  this.multasProv.municipio_selected: null;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListMunicipioTestePage');
    this.getListMunicipio();
  }

  async getListMunicipio() {
    let estado_id:number = await this.navParams.get('uf_emplacamento');

    this.muniProv.filterByEstate(estado_id).then((res: IMunicipio[]) => {
      this.municipios = res;
      console.log('res filterByEstate', res);
      this.paginate();
    });
  }

  paginate(){
    this.listItems(this.municipios, 1, 20).then((arrayPaginate) => {
      this.municipio_array = arrayPaginate.data;

      this.municipio_array.forEach(element => {
        if(this.item_selected){
          if(element.id == this.item_selected.id){
            //elemento salvo recebe true
            element.selected = true;
          } else {
            //demais elementos false
            element.selected = false;
          }
        } else {
          //todos os elementos recebem false
          element.selected = false;
        }
      });

      this.next_page = arrayPaginate.next_page;
      this.total_page = arrayPaginate.total_page;
      console.log('total_page: ', this.total_page, ' page_atual: 1');
    })
  }

  private listItems(items: Array<any>, pageActual:number, limitItems:number): Promise<any> {

    return new Promise((resolve, reject) => {
      let result = [];
      let totalPage = Math.ceil(items.length/limitItems);
      let count = (pageActual * limitItems) - limitItems;
      let delimiter = count + limitItems;
  
      if(pageActual <= totalPage){
        //TODO: Create loop
        for(let i=count; i<delimiter; i++){
          if(items[i] != null){
            //TODO: Push in Result
            result.push(items[i]);
          }
          //TODO: increment count
          count++;
        }
      }
  
      let arrayPaginate = {
        data: result,
        total_page: totalPage,
        next_page: this.next_page > totalPage? null: pageActual + 1
      }

      resolve(arrayPaginate)
    });

  }

  selectItem(item, checkbox: Checkbox){
    console.log('checkbox', checkbox, ' current_checkbox', this.multasProv.current_checkbox_municipio)
    if(this.multasProv.municipio_selected.id != ''){
      this.multasProv.current_checkbox_municipio.checked == true? false: true;
      this.multasProv.municipio_selected = item;
    } else {
      this.multasProv.municipio_selected = item;
    }
    this.multasProv.current_checkbox_municipio = checkbox;
    this.viewCtrl.dismiss({ item });

    
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    console.log('totalPage', this.total_page, 'next_page', this.next_page, 'length', this.municipio_array.length);

    if(this.next_page){
      this.listItems(this.municipios, this.next_page, 20).then((res) => {

        let new_array = res.data;
        this.next_page = res.next_page;
        this.total_page = res.total_page

        new_array.forEach(element => {
          if(this.item_selected){
            if(element.id == this.item_selected.id){
              //elemento salvo recebe true
              element.selected = true;
              this.municipio_array.push(element)
            } else {
              //demais elementos false
              element.selected = false;
              this.municipio_array.push(element)
            }
          } else {
            //todos os elementos recebem false
            element.selected = false;
            this.municipio_array.push(element)
          }
          
        })
        
        infiniteScroll.complete();
      })
    } else {
      infiniteScroll.complete();
    }

  }

  async getItemsFilter(ev: any) {
    // set val to the value of the searchbar
    const val = ev.target.value;

    console.log('val1', val)

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.view_loading = true;
      this.array_filter = 3;

      const filterItems = (query) => {
        return this.municipios.filter(el => el.nome.toLowerCase().indexOf(query.toLowerCase()) > -1);
      };

      console.log('enquadramentos', filterItems(val));

      this.municipio_array_filter = filterItems(val);
      
      this.municipio_array_filter.forEach(element => {
        if(this.multasProv.enquadramento_selected.id != ""){
          if(element.id == this.multasProv.enquadramento_selected.id){
            //elemento salvo recebe true
            element.selected = true;
          } else {
            //demais elementos false
            element.selected = false;
          }
        } else {
          //todos os elementos recebem false
          element.selected = false;
        }
      }) 

      this.listItems(this.municipio_array_filter, 1, 20).then((res) => {
          this.municipio_array_filter_paginate = res.data;
          this.municipio_array_filter_next_page = res.next_page;
          console.log('filter res.next_page', res.next_page, 'res.total_page', res.total_page)
          this.municipio_array_filter_total_page = res.total_page;
          this.array_filter = true;
          this.view_loading = false
      })
    } else {
      this.array_filter = false;
    }
  }

  getItemsDoInfinite(infiniteScroll) {

    if(this.municipio_array_filter_next_page !== null ){
      this.listItems(this.municipio_array_filter, this.municipio_array_filter_next_page, 20).then((res) => {
        let new_array = res.data;
        this.municipio_array_filter_next_page = res.next_page;

        new_array.forEach(element => {
          if(this.multasProv.enquadramento_selected.id != ""){
            if(element.id == this.multasProv.enquadramento_selected.id){
              //elemento salvo recebe true
              element.selected = true;
              this.municipio_array_filter_paginate.push(element)
            } else {
              //demais elementos false
              element.selected = false;
              this.municipio_array_filter_paginate.push(element)
            }
          } else {
            //todos os elementos recebem false
            element.selected = false;
            this.municipio_array_filter_paginate.push(element)
          }
        })

        infiniteScroll.complete();
      })
    } else {
      infiniteScroll.complete();
    }  

  }

  onCancel(){
    this.array_filter = false;
  }

  closeModal(){
    this.viewCtrl.dismiss()
  }

}
