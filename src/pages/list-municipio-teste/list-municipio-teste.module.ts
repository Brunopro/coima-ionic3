import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListMunicipioTestePage } from './list-municipio-teste';

@NgModule({
  declarations: [
    ListMunicipioTestePage,
  ],
  imports: [
    IonicPageModule.forChild(ListMunicipioTestePage),
  ],
})
export class ListMunicipioTestePageModule {}
