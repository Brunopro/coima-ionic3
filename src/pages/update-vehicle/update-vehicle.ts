import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { VehicleProvider } from '../../providers/vehicle/vehicle';
import { PreferencesProvider } from '../../providers/preferences/preferences';

@IonicPage()
@Component({
  selector: 'page-update-vehicle',
  templateUrl: 'update-vehicle.html',
})
export class UpdateVehiclePage {
  public registerVehicle : FormGroup;
  load: Loading;
  uid:string

  vehicle = {
    placa: '',
    renavan: '',
    uid: ''
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public vehicleProv: VehicleProvider,
    public loadindCtrl: LoadingController,
    public preferencesProv: PreferencesProvider) {

    this.vehicle = this.navParams.get('vehicle')

    this.registerVehicle = this.formBuilder.group({
      placa: [this.vehicle.placa, Validators.compose([Validators.required, Validators.minLength(8)])],
      renavan: [this.vehicle.renavan, Validators.compose([Validators.required, Validators.minLength(11)])],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddVehiclePage');
  }

  saveVehicle(){

    this.loadingPresent()


    let vehicle = {
      placa: this.registerVehicle.controls.placa.value.toUpperCase(),
      renavan: this.registerVehicle.controls.renavan.value,
      uid: this.vehicle.uid,
    }

    this.vehicleProv.updateVehicle(vehicle).then(() =>  {
      console.log('success!');

      let index = this.preferencesProv.vehicle.indexOf(this.vehicle);
      console.log('update', index)

      //atualizando array
      //remove item da posição
      this.preferencesProv.vehicle.splice(index, 1);

      //adiciona item atualizado na antiga posição
      this.preferencesProv.vehicle.splice(index, 0, vehicle);

      //atualiza o storage com novos dados
      this.preferencesProv.create('vehicle', this.preferencesProv.vehicle).then(()=>  {
        this.load.dismiss()
        this.navCtrl.setRoot('MyVehiclePage')
      })
    
    }).catch(err => {
      this.load.dismiss()
      console.log('error', err)
    })
  }

  loadingPresent(){
    this.load = this.loadindCtrl.create({
      content: 'Salvando dados, aguarde..'
    })

    this.load.present()
  }


}
