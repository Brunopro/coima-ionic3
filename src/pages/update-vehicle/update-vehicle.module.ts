import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateVehiclePage } from './update-vehicle';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    UpdateVehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateVehiclePage),
    BrMaskerModule
  ],
})
export class UpdateVehiclePageModule {}
