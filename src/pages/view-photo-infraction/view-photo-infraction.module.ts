import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewPhotoInfractionPage } from './view-photo-infraction';

@NgModule({
  declarations: [
    ViewPhotoInfractionPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewPhotoInfractionPage),
  ],
})
export class ViewPhotoInfractionPageModule {}
