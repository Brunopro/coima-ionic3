import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ViewPhotoInfractionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-photo-infraction',
  templateUrl: 'view-photo-infraction.html',
})
export class ViewPhotoInfractionPage {

  public foto_infracao:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.foto_infracao = navParams.get('foto_infracao') ? navParams.get('foto_infracao') : 'https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png' ;
    console.log('this.foto_infracao', this.foto_infracao)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewPhotoInfractionPage');
  }

  modalDismiss(){
    this.viewCtrl.dismiss();
  }

}
