import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListEnquadramentoTestePage } from './list-enquadramento-teste';

@NgModule({
  declarations: [
    ListEnquadramentoTestePage,
  ],
  imports: [
    IonicPageModule.forChild(ListEnquadramentoTestePage),
  ],
})
export class ListEnquadramentoTestePageModule {}
