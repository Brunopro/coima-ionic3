import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Checkbox, Platform } from 'ionic-angular';
import { EnquadramentoProvider } from '../../providers/enquadramento/enquadramento';
import { MultasProvider } from '../../providers/multas/multas';
import { IEnquadramento } from '../../interfaces/IEnquadramento';
import { SQLite } from '@ionic-native/sqlite';

@IonicPage()
@Component({
  selector: 'page-list-enquadramento-teste',
  templateUrl: 'list-enquadramento-teste.html',
})

export class ListEnquadramentoTestePage {

  enquadramentos:Array<any>;

  enquadramento_array:Array<any>;
  next_page:number;
  total_page:number;
  array_filter: any = false;
  enquadramento_array_filter:any;
  item_selected: any;
  view_loading: boolean = false;
  enquadramento_array_filter_next_page: any;
  body = {
    code: ''
  };

  enquadramento_array_paginate: Array<any>;
  enquadramento_array_filter_total_page: any;
  enquadramento_array_filter_paginate: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public multasProv: MultasProvider,
    public viewCtrl: ViewController,
    public enquaProv: EnquadramentoProvider,
    private plt: Platform,
    private sqlite: SQLite) {

      this.item_selected =  this.multasProv.enquadramento_selected?  this.multasProv.enquadramento_selected: null;
  
  }

  ionViewDidLoad(){
    this.getData();
  }

  getData(){
    this.plt.ready().then(() => {
      this.enquaProv.getAll().then((enquadramentos: IEnquadramento[]) => {       
        this.enquadramentos = enquadramentos;
        console.log('enquadramentos', enquadramentos.length);
        this.paginate();
      });
    });
  }

  paginate(){
    this.listItems(this.enquadramentos, 1, 20).then((arrayPaginate) => {
      this.enquadramento_array = arrayPaginate.data;

      this.enquadramento_array.forEach(element => {
        if(this.item_selected){
          if(element.id == this.item_selected.id){
            //elemento salvo recebe true
            element.selected = true;
          } else {
            //demais elementos false
            element.selected = false;
          }
        } else {
          //todos os elementos recebem false
          element.selected = false;
        }
      })

      this.next_page = arrayPaginate.next_page;
      this.total_page = arrayPaginate.total_page;
      console.log('total_page: ', this.total_page, ' page_atual: 1');
    })
  }

  private listItems(items: Array<any>, pageActual:number, limitItems:number): Promise<any>{

    return new Promise((resolve, reject) => {
      let result = [];
      let totalPage = Math.ceil(items.length/limitItems);
      let count = (pageActual * limitItems) - limitItems;
      let delimiter = count + limitItems;
  
      if(pageActual <= totalPage){
        //TODO: Create loop
        for(let i=count; i<delimiter; i++){
          if(items[i] != null){
            //TODO: Push in Result
            result.push(items[i]);
          }
          //TODO: increment count
          count++;
        }
      }
  
      let arrayPaginate = {
        data: result,
        total_page: totalPage,
        next_page: this.next_page > totalPage? null: pageActual + 1
      }

      resolve(arrayPaginate)
    });

  }

  selectItem(item, checkbox: Checkbox){
    console.log('item enquadramento', item)
    console.log('checkbox', checkbox, ' this.current_checkbox', this.multasProv.current_checkbox_enquadramento)
    if(this.multasProv.enquadramento_selected.id != ''){
      this.multasProv.current_checkbox_enquadramento.checked == true? false: true
      this.multasProv.enquadramento_selected = item;
     
    } else {
      this.multasProv.enquadramento_selected = item;
  
    }

    this.multasProv.current_checkbox_enquadramento = checkbox;
    
    this.viewCtrl.dismiss({ item });
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    console.log('totalPage', this.total_page, 'next_page', this.next_page, 'length', this.enquadramento_array.length);

    if(this.next_page){
      this.listItems(this.enquadramentos, this.next_page, 20).then((res) => {

        let new_array = res.data;
        this.next_page = res.next_page;
        this.total_page = res.total_page

        new_array.forEach(element => {
          if(this.item_selected){
            if(element.id == this.item_selected.id){
              //elemento salvo recebe true
              element.selected = true;
              this.enquadramento_array.push(element)
            } else {
              //demais elementos false
              element.selected = false;
              this.enquadramento_array.push(element)
            }
          } else {
            //todos os elementos recebem false
            element.selected = false;
            this.enquadramento_array.push(element)
          }
          
        })
        
        infiniteScroll.complete();
      })
    } else {
      infiniteScroll.complete();
    }

  }

  async getItemsFilter(ev: any) {
    // set val to the value of the searchbar
    const val = ev.target.value;

    console.log('val1', val)

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.view_loading = true;
      this.array_filter = 3;

      const filterItems = (query) => {
        return this.enquadramentos.filter(el => el.descricao.toLowerCase().indexOf(query.toLowerCase()) > -1);
      };

      console.log('enquadramentos', filterItems(val));

      this.enquadramento_array_filter = filterItems(val);
      
      this.enquadramento_array_filter.forEach(element => {
        if(this.multasProv.enquadramento_selected.id != ""){
          if(element.id == this.multasProv.enquadramento_selected.id){
            //elemento salvo recebe true
            element.selected = true;
          } else {
            //demais elementos false
            element.selected = false;
          }
        } else {
          //todos os elementos recebem false
          element.selected = false;
        }
      }) 

      this.listItems(this.enquadramento_array_filter, 1, 20).then((res) => {
          this.enquadramento_array_filter_paginate = res.data;
          this.enquadramento_array_filter_next_page = res.next_page;
          console.log('filter res.next_page', res.next_page, 'res.total_page', res.total_page)
          this.enquadramento_array_filter_total_page = res.total_page;
          this.array_filter = true;
          this.view_loading = false
      })
    } else {
      this.array_filter = false;
    }
  }

  getItemsDoInfinite(infiniteScroll) {

    if(this.enquadramento_array_filter_next_page !== null ){
      this.listItems(this.enquadramento_array_filter, this.enquadramento_array_filter_next_page, 20).then((res) => {
        let new_array = res.data;
        this.enquadramento_array_filter_next_page = res.next_page;

        new_array.forEach(element => {
          if(this.multasProv.enquadramento_selected.id != ""){
            if(element.id == this.multasProv.enquadramento_selected.id){
              //elemento salvo recebe true
              element.selected = true;
              this.enquadramento_array_filter_paginate.push(element)
            } else {
              //demais elementos false
              element.selected = false;
              this.enquadramento_array_filter_paginate.push(element)
            }
          } else {
            //todos os elementos recebem false
            element.selected = false;
            this.enquadramento_array_filter_paginate.push(element)
          }
        })

        infiniteScroll.complete();
      })
    } else {
      infiniteScroll.complete();
    }  

  }

  onCancel(){
    this.array_filter = false;
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }


}
