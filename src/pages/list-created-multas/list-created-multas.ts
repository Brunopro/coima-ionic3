import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, LoadingController, AlertController, PopoverController, Platform, Loading } from 'ionic-angular';
import { MultasProvider } from '../../providers/multas/multas';
import moment from 'moment'
import { IMulta } from '../../interfaces/IMulta';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { OverlayProvider } from '../../providers/overlay/overlay';
import { SQLiteObject } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
@IonicPage()
@Component({
  selector: 'page-list-created-multas',
  templateUrl: 'list-created-multas.html',
})
export class ListCreatedMultasPage {

  public changeViewFilterMultas:string = 'multas_enviadas';

  item_select_long_press: boolean = false;
  private cpf:string;
  private user = {
    cpf: '',
    company: ''
  };
  load:Loading;
  isConnect:boolean;


  itemExpandHeight: number = 200;

  listNames = [
    {id: 1, name: 'Lorem ', desc:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed metus a enim consequat dapibus eget sit amet magna. Quisque vestibulum, velit sit amet dictum pharetra', andress: 'Rua teste', valor: 'R$ 0,00'},
  ]

  //multas = [{placa:' ', data_infracao: ['00/00/0000 '], valor_recebido: 0,  marca_e_modelo: '', observacao: '', expanded: false}]

  multas = [];

  next_page_url:string;


  //listando multas do sqlite
  
  item_select_long_press_multa_sqlite: boolean = false;
  itemExpandHeightMultaSqlite: number = 200;
  multasSqlite: IMulta[] = [];
  multas_paginateSqlite: IMulta[] = [];
  next_page;
  total_page;
  //fim - listando multas do sqlite

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public multasProv: MultasProvider,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public popoverCtrl: PopoverController,
    private platform: Platform,
    private preferencesProv: PreferencesProvider,
    private overlayProv: OverlayProvider,
    public network: Network) {
      this.isConnect = this.network.type == 'none'? false : true;
  }

  ionViewDidLoad(){
    this.platform.ready().then(() => {
      
      this.getMultas();
      this.getMultasSqlite();


    });
  }

  updateDataPage(){
    this.getMultas();
    this.getMultasSqlite();
  }

  getMultasSqlite(){
    return this.multasProv.getAll()
    .then((multas: IMulta[]) => {
      console.log('setando multas do sqlite ', multas)
      if(multas.length > 0){
        this.multas_paginateSqlite = [];
        this.multasSqlite = [];
        this.multasSqlite = multas;
        this.paginate();
      } else{
        this.multasSqlite = [];
        this.multas_paginateSqlite = []
      }
      
    })
    .catch((err: Error) => {
      console.log('Não foi possivel recuperar dados do sqlite')
    })
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create('PopoverFilterMultaPage', {self:this});
    popover.present({
      ev: myEvent
    });
  }

  getMultas(){
    if(this.multasProv.connection == true){
      this.isConnect = true;

      let load = this.loadingCtrl.create({
        content: 'aguarde...'
      })
  
      load.present()

      this.preferencesProv.get('user').then((user) => {
        this.user = {
          cpf: user.cpf.replace(".", "").replace(".", "").replace("-", ""),
          company: user.company
        }
        console.log('user do storage', this.user);
  
        this.multasProv.getAllMultasByAgente(this.user).subscribe((res) => {
          console.log('res', res);
          load.dismiss();
          
          if(res["Erro"] == "unable to retrieve the data"){
            this.multas = [];
          } else {
            this.multas = res["data"];
            this.next_page_url = res["next_page_url"];
      
            this.multas.forEach(element => {
              let data_infracao  =  element["data_infracao"].split(' ', 2);
      
              console.log('data_infracao split', data_infracao)
      
              //convertendo data do formato americano para br
              element["data_infracao_br"] =  moment(element["data_infracao"], 'YYYY-MM-DD').format('DD/MM/YYYY');
            });
          }
          
        }, err => {
          this.multas = [];
          console.log('err', err);
          load.dismiss();
          if(this.network.type == 'none'){
            this.isConnect = false;
          } else {
            this.overlayProv.alertPresent({message: 'Falha ao consultar dados'});
          }
        })
      });
    } else {
      this.isConnect = false;
      this.multas = [];
    }  


  }

  setColor(i){

    if((i % 2) === 0){
      return 'gray'
    } else {
      return 'default'
    }
  }


  expandItem(item) {

    if(item.valor_recebido){
    console.log('item', item.valor_recebido.toFixed(2))
    }
    this.multas.map((listItem) => {

      if (item == listItem) {
        listItem.expanded = !listItem.expanded;
      } else {
        listItem.expanded = false;
      }

      return listItem;

    });

  }

  pressed(){
    console.log('pressed')
  }

  async  openActionSheet(multa){
    const actionsheet = await this.actionSheetCtrl.create({
      title:"Opções",
      buttons:[{
      text: 'Editar',
      icon: 'create',
      cssClass: 'EditionIcon',
      handler: () => {
        console.log('Editar', multa)
        if(this.network.type != 'none'){
          if(multa.situacao_multas_id  == 1){
            this.navCtrl.push('UpdateMultaPage', {multa: multa});
          } else {
            this.alertCtrl.create({
              title: 'Operação não permitida',
              subTitle: 'Só é possível editar multa se a situação estiver pedente para envio 411',
              buttons: ['Entendi']
            }).present();
          }
        } else {
          this.overlayProv.alertPresent({title: 'Falha', message: 'Verifique sua conexão a internet'});
        }
      }
      },
      {
        text: 'Delete',
        icon: 'trash',
        cssClass: 'EditionIcon',
        handler: () => {
          console.log('excluir', multa);
          if(this.network.type != 'none'){
            if(multa.situacao_multas_id == 1){
              this.presentConfirmDelete(multa.id)
            } else {
              this.alertCtrl.create({
                title: 'Operação não permitida',
                message: 'Só é possível excluir multa se a situação estiver pedente para envio 411',
                buttons: ['Entendi']
              }).present();
            }
          } else {
            this.overlayProv.alertPresent({title: 'Falha', message: 'Verifique sua conexão a internet'});
          }
        }
      }]
    });
    await actionsheet.present();
  }




  presentConfirmDeleteCriatedMulta(id){
    let alert = this.alertCtrl.create({
      title: 'Confirmação',
      message: 'Deseja mesmo excluir essa multa?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            console.log('Buy clicked')
            this.multasProv.deleteMultaInApi(id,this.user).subscribe(res =>  {
              console.log('res delete multa', res)

              this.getMultas()

            }, err  => {
              console.log('err', err)
            })
          }
        }
      ]
    });
    alert.present();
  }

  //Submit Multas Sqlite
  submitMultasOffline(){
    let load = this.overlayProv.loadPresent('Enviando multa, aguarde..');

    console.log('enviar', this.multasSqlite);
    let user = {
      cpf: this.user.cpf.replace(".", "").replace(".", "").replace("-", ""),
      company: this.user.company
    }
    this.multasProv.saveMultaInApi(this.multasSqlite, user).subscribe(async(res) => { //mandar user
      console.log('res saveMultaInApi', res)

      if(res){
        let multas_salvas = [];
        multas_salvas = res["multas_salvas"];
        let multas_existentes:IMulta[];
        multas_existentes = res["multas_existentes"];
        console.log('multas', multas_salvas)
        console.log('multas existentes', multas_existentes)
        if(multas_salvas.length > 0){

          let array_query = [];
          for (let i = 0; i < multas_salvas.length; i++) {

            array_query.push([
              `DELETE FROM multas where id = ${multas_salvas[i]["id_multa_sqlite"]}`, []
            ]);

            console.log('array_query', array_query, 'multas_salvas.length - 1', multas_salvas.length - 1, 'i', i)
            if(i == (multas_salvas.length - 1)){
              console.log('atualizando multas sqlite')
              this.multasProv.getDB().then((db:SQLiteObject) => {

                console.log('array_query', array_query, 'db ', db)
                db.sqlBatch(array_query)
                  .then(() => {
                    if(multas_existentes.length > 0){
                      this.multasInvalidas(multas_existentes).then(() => {
                        load.dismiss()
                        this.getMultasSqlite();
                      })
                    } else {
                      this.getMultasSqlite();
                      load.dismiss()
                    }
                    
                    console.log('multas envidas excluidas do sqlite com sucesso')
                  }).catch((e) => {
                    load.dismiss();
                    console.log('erro ao excluir multas enviadas do sqlite',e)
                  })
              })
            }
          }

         

          this.overlayProv.toastPresent({
            message: `Multas enviadas: salvas com sucesso (${multas_salvas.length}), falha ao salvar (${multas_existentes.length}) numero auto infração duplicado`,
            duration: 30000
           })          
        } else {
          if(multas_existentes.length > 0){
          console.log('contem multas no sql duplicadas que não podem ser salvas')
          this.multasInvalidas(multas_existentes).then((result) => {
            this.getMultasSqlite();
            load.dismiss();
            console.log('result delete', result)
          }).catch((e) => {
            console.error('error delte', e);
            load.dismiss();
          });

          this.overlayProv.toastPresent({
            message: `Multas enviadas: salvas com sucesso (${multas_salvas.length}), falha ao salvar (${multas_existentes.length}) numero auto infração duplicado`,
            duration: 30000
          })
          }
        }

      } else {
        console.log('sem resposta');
        load.dismiss();
      }
    }, err => {
      load.dismiss();
      console.log('error ao savar multas não enviadas a api', err);
    })
  }

  multasInvalidas(multas_existentes):Promise<any>{
    console.log('multas_existentes', multas_existentes);
    return this.multasProv.deleteDuplicateMultas(multas_existentes);
  }

  getMoreMultas(infiniteScroll) {
    if(this.next_page_url){
      this.multasProv.getMoreMultas(this.next_page_url, this.user).subscribe(res =>  {
        let new_array = res["data"];
  
        new_array.forEach(element => {
          let data_infracao =  element["data_infracao"].split(' ', 2);
          
          //element["data_infracao"] =  element["data_infracao"].split(' ', 1);

          

          console.log('data_infracao split', data_infracao)

          element["data_infracao_br"] =  moment(element["data_infracao"], 'YYYY-MM-DD').format('DD/MM/YYYY');
  
          this.multas.push(element);
        });
  
        this.next_page_url = res["next_page_url"]
  
        infiniteScroll.complete();
      }, err =>  {
        infiniteScroll.complete();
        console.log('error', err)
      })
    } else {
      infiniteScroll.complete();
    }
  }

 
  buttonFooter(item){
    console.log('Clicado')
    this.navCtrl.push('PrintMultaPage', {multa: item})
  }

  presentConfirmDelete(id){
    let alert = this.alertCtrl.create({
      title: 'Confirmação',
      message: 'Deseja mesmo excluir essa multa?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.load = this.overlayProv.loadPresent();
            console.log('Buy clicked')
            this.multasProv.deleteInApi(id, this.user).subscribe(res =>  {
              this.load.dismiss();
              console.log('res delete multa', res)
              this.getMultas();
            }, err  => {
              this.load.dismiss();
              console.log('err', err)
              this.overlayProv.alertPresent({title: 'Erro', message: 'Ocorreu um erro ao excluir'});
            })
          }
        }
      ]
    });
    alert.present();
  }

  //Listando multas criadas no sqlite

  paginate(){
    this.listItems(this.multasSqlite, 1, 20).then((arrayPaginate) => {
      let multas_paginate = arrayPaginate.data;

      multas_paginate.forEach(element => {
        let data_infracao  =  element["data_infracao"].split(' ', 2);

        console.log('data_infracao split', data_infracao)
        //convertendo data do formato americano para br
        element["data_infracao_br"] =  moment(element["data_infracao"], 'YYYY-MM-DD').format('DD/MM/YYYY');

        this.multas_paginateSqlite.push(element);
      });

      this.next_page = arrayPaginate.next_page;
      this.total_page = arrayPaginate.total_page;
      console.log('total_page: ', this.total_page, ' page_atual: 1');
    });
  }

  private listItems(items: Array<any>, pageActual:number, limitItems:number): Promise<any> {

    return new Promise((resolve, reject) => {
      let result = [];
      let totalPage = Math.ceil(items.length/limitItems);
      let count = (pageActual * limitItems) - limitItems;
      let delimiter = count + limitItems;
  
      if(pageActual <= totalPage){
        //TODO: Create loop
        for(let i=count; i<delimiter; i++){
          if(items[i] != null){
            //TODO: Push in Result
            result.push(items[i]);
          }
          //TODO: increment count
          count++;
        }
      }
  
      let arrayPaginate = {
        data: result,
        total_page: totalPage,
        next_page: this.next_page > totalPage? null: pageActual + 1
      }

      resolve(arrayPaginate)
    });

  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    console.log('totalPage', this.total_page, 'next_page', this.next_page, 'length', this.multas_paginateSqlite.length);

    if(this.next_page){
      this.listItems(this.multasSqlite, this.next_page, 20).then((res) => {

        let new_array = res.data;
        this.next_page = res.next_page;
        this.total_page = res.total_page

        new_array.forEach(element => {
          let data_infracao  =  element["data_infracao"].split(' ', 2);
          console.log('data_infracao split', data_infracao)
          //convertendo data do formato americano para br
          element["data_infracao_br"] =  moment(element["data_infracao"], 'YYYY-MM-DD').format('DD/MM/YYYY');  
          this.multas_paginateSqlite.push(element);
        });
        
        infiniteScroll.complete();
      })
    } else {
      infiniteScroll.complete();
    }

  }

  released(){
    console.log('released')
  }

   /*Não pode imprimir multa ainda n enviada
  buttonFooter(item){
    console.log('Clicado')
    this.navCtrl.push('PrintMultaPage', {multa: item})
  }
  */

  pushRegisterMulta(){
    this.navCtrl.push('RegisterMultaPage');
  }

  expandItemMultaSqlite(item) {

    if(item.valor_recebido){
    console.log('item', item.valor_recebido.toFixed(2))
    }
    this.multasSqlite.map((listItem) => {

      if (item == listItem) {
        listItem.expanded = !listItem.expanded;
      } else {
        listItem.expanded = false;
      }

      return listItem;

    });

  }


  async  openActionSheetMultaSqlite(multa){
    const actionsheet = await this.actionSheetCtrl.create({
      title:"Opções",
      buttons:[
      /*{
      text: 'Editar',
      icon: 'create',
      cssClass: 'EditionIcon',
      handler: () => {
        console.log('Editar', multa)
        if(multa.status == 1){
          this.navCtrl.push('UpdateMultaPage', {multa: multa});
        } else {
          this.alertCtrl.create({
            title: 'Alerta',
            subTitle: 'Operação não permitida, somente é permitido editar multas com status iguais a 1',
            buttons: ['Entendi']
          }).present();
        }
      }
      },*/
      {
        text: 'Delete',
        icon: 'trash',
        cssClass: 'EditionIcon',
        handler: () => {
          console.log('excluir', multa)
          this.presentConfirmDeleteSqlite(multa.id)
        }
      }]
    });
    await actionsheet.present();
  }

  presentConfirmDeleteSqlite(id){
    let alert = this.alertCtrl.create({
      title: 'Confirmação',
      message: 'Deseja mesmo excluir essa multa?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            console.log('Buy clicked')
            this.multasProv.delete(id).then(res =>  {
              console.log('res delete multa', res)
              this.load = this.overlayProv.loadPresent();

              this.getMultasSqlite().then(() => {
                this.load.dismiss();
              }).catch((err) => {
                console.log('err', err);
                this.load.dismiss();
              })

            }, err  => {
              console.log('err', err)
            })
          }
        }
      ]
    });
    alert.present();
  }



}
