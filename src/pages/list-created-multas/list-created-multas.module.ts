import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from '../../components/components.module';
import { LongPressModule } from 'ionic-long-press';
import { ListCreatedMultasPage } from '../list-created-multas/list-created-multas'

@NgModule({
  declarations: [
    ListCreatedMultasPage,
  ],
  imports: [
    ComponentsModule,
    LongPressModule,
    IonicPageModule.forChild(ListCreatedMultasPage),
  ],
  entryComponents: [
  ]
})
export class ListCreatedMultasPageModule {}
