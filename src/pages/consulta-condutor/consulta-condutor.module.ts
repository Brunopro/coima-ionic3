import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsultaCondutorPage } from './consulta-condutor';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    ConsultaCondutorPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsultaCondutorPage),
    BrMaskerModule
  ],
})
export class ConsultaCondutorPageModule {}
