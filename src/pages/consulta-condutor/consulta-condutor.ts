import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, IonicFormInput, LoadingController, Loading, Platform } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebServiceSoapMaProvider } from '../../providers/web-service-soap-ma/web-service-soap-ma';
import moment from 'moment';
import { PreferencesProvider } from '../../providers/preferences/preferences';

@IonicPage()
@Component({
  selector: 'page-consulta-condutor',
  templateUrl: 'consulta-condutor.html',
})
export class ConsultaCondutorPage {

  //public query_types = [{id:4, name:'CPF'},{id:5, name:'Registro'},{id:6, name: 'Numero CNH'}, {id:7, name: 'PGU'}];
  public query_types = [{id:7, name:'CPF'},{id:2, name:'Registro'},{id:3, name: 'Numero CNH'}, {id:5, name: 'PGU'}];
  public toppings = {
    id: '',
    name: ''
  };
  public search_value:string = " ";

  public retorno:any;

  public formCondutor:FormGroup;
  private load:Loading;
  private user:any; 

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder:FormBuilder,
    public alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public webService: WebServiceSoapMaProvider,
    private preferenceProv: PreferencesProvider,
    private platform: Platform) {
      this.platform.ready().then(() => {
        this.preferenceProv.get('user').then((user) => {
          this.user ={
            cpf: user.cpf.replace(".", "").replace(".", "").replace("-", ""),
            company: user.company
          } 
        })
      });

      this.formCondutor = formBuilder.group({
        tipo: ['', Validators.required],
        chave: ['', Validators.required]
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsultaCondutorPage');
  }

  
  onSubmit(){

    this.load = this.loadingCtrl.create({
      content: 'aguarde..'
    });

    this.load.present();



    console.log(this.formCondutor.get('chave').value['name'])
    if(this.formCondutor.valid == false){
      console.log('if', this.formCondutor.value)
      this.alertCtrl.create({
        message: 'Preencha os campos corretamente'
      }).present();
      
      return
    }

    console.log('tipo', this.formCondutor.controls.tipo.value)

    if(this.formCondutor.controls.tipo.value.id == 7){
      this.formCondutor.controls.chave.setValue(this.formCondutor.controls.chave.value.replace(".", "").replace(".", "").replace("-", ""));

      this.webService.consulta({data:this.formCondutor.value, tipo: '2', user:this.user})
      .subscribe((res) => {
        console.log(res)
        this.filterResult(res);
        this.load.dismiss();
      }, err => {
        console.log(err)
        if(err.message == "Error 1014 0954: Natural RPC Server returns: NJXMN001 3010 NAT0954 Abnormal termination S0C7 during program execution., NE=01,NJXMN001301002O"){
          console.log('dado enviado invalido')
        }
        this.alertPresent({title: 'Informativo', message: 'Servidor do Detran-MA, indisponível no momento.'})
        this.load.dismiss();
      })
    } else {
      this.webService.consulta({data:this.formCondutor.value, tipo: '2', user:this.user}).subscribe((res) => {
        console.log(res)
        this.filterResult(res);
        this.load.dismiss();
      }, err => {
        console.log(err)
        if(err.message == "Error 1014 0954: Natural RPC Server returns: NJXMN001 3010 NAT0954 Abnormal termination S0C7 during program execution., NE=01,NJXMN001301002O"){
          console.log('dado enviado invalido')
        }
        this.alertPresent({title: 'Informativo', message: 'Servidor do Detran-MA, indisponível no momento.'})
        this.load.dismiss();
      })
    }



    

    console.log('passou no teste', this.formCondutor.valid);
  }

  public changeType(){
    console.log('change value', this.formCondutor.controls.tipo.value);

    this.formCondutor.controls.chave.setValue('');


  }

  private filterResult(result):void {

    if(result['WS_RETORNO'] == 'L001'){
      this.filterL001(result)
      //if(result['WS_RETORNO'] == 'N002' || result['WS_RETORNO'] == 'N732' || result['WS_RETORNO'] == 'L002' || result['WS_RETORNO'] == 'N836' || result['WS_RETORNO'] == 'N033' || result['WS_RETORNO'] == 'N830' || result['WS_RETORNO'] == 'N752' )
    } else {
      this.filterFalha(result)
    } 
  }

  private filterL001(result):void{
    this.retorno = {
      mensagem: result['WS_MENSAGEM'],
      data: [
      {title: 'NOME', data: result['WS_NOME']},
      {title: 'GENERO', data: result['WS_GENERO'] == '1'? 'MASCULINO': 'FEMININO'},
      {title: 'CATEGORIA CNH', data: result['WS_CATEGORIA'] == "" ? 'SEM REGISTRO': result['WS_CATEGORIA']},
      {title: 'Nº CNH', data: result['WS_CNH'] == "" ? 'SEM REGISTRO': result['WS_CNH']},
      {title: 'Nº PGU', data: result['WS_PGU'] == "" ? 'SEM REGISTRO': result['WS_PGU']},
      {title: 'Nº DO PRONTUÁRIO', data: result['WS_PROCON'] == "" ? 'SEM REGISTRO': result['WS_PROCON']},
      {title: 'Nº RENACH', data: result['WS_RENACH'] == "" ? 'SEM REGISTRO': result['WS_RENACH'] },
      {title: 'SITUACAO CNH', data: result['WS_SITUACAO'] == "" ? 'SEM REGISTRO': result['WS_SITUACAO']},
      {title: 'DATA NASCIMENTO', data: result['WS_DATNASC']? moment(result['WS_DATNASC'], "YYYYMMDD").format('DD/MM/YYYY') : "SEM REGISTRO"},
      {title: 'DATA ULTÍMA EMISSÃO', data: result['WS_EMISSAO'] ? moment(result['WS_EMISSAO'], "YYYYMMDD").format('DD/MM/YYYY') : "SEM REGISTRO"},
      {title: 'DATA PRIMEIRA HABILITAÇÃO', data: result['WS_1HAB'] ? moment(result['WS_1HAB'], "YYYYMMDD").format('DD/MM/YYYY') : "SEM REGISTRO"},
      {title: 'VALIDADE CNH', data: result['WS_VALIDADE'] ? moment(result['WS_VALIDADE'], "YYYYMMDD").format('DD/MM/YYYY') : "SEM REGISTRO"}
    ]}
  }

  private filterFalha(result):void {
    this.retorno = null;
    this.alertPresent({message:result['WS_MENSAGEM']})
  }

  alertPresent(params:{title?:string, message:string}):void {
    this.alertCtrl.create({
      title: params.title?params.title:'Falha',
      message: params.message,
      buttons: ['OK']
    }).present()
  }

}
