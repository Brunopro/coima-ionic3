import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Loading, LoadingController, AlertController } from 'ionic-angular';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { HttpClient } from '@angular/common/http';
import { EnquadramentoProvider } from '../../providers/enquadramento/enquadramento';
import { MultasProvider } from '../../providers/multas/multas';

@IonicPage()
@Component({
  selector: 'page-list-municipio',
  templateUrl: 'list-municipio.html',
})
export class ListMunicipioPage {

  municipio_array = [];
  next_page_url:string;
  array_filter: any = false;
  municipio_array_filter = [];
  item_selected: any;
  view_loading: boolean = false;
  municipio_array_filter_next_page: any;
  body = {
    uf_emplacamento: ''
  };

  body_filter = {
    nome: '',
    uf_emplacamento: ''
  }

  uf_emplacamento: any;
  load: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public preferencesProv: PreferencesProvider,
    public viewCtrl: ViewController,
    public http: HttpClient,
    public enquaProv: EnquadramentoProvider,
    public multaProv: MultasProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController) {

      this.loadingPresent()
      console.log('enquadramento_selected', this.multaProv.municipio_selected)
      this.item_selected =  this.multaProv.municipio_selected?  this.multaProv.municipio_selected: null;
      this.body.uf_emplacamento = this.navParams.get('uf_emplacamento');
      this.uf_emplacamento = this.navParams.get('uf_emplacamento');
      console.log('uf_emplacamento', this.uf_emplacamento)


      this.getMunicipios()
  }

  getMunicipios(){
    this.multaProv.getMunicipios(this.body).subscribe(data => {
      console.log('data getMunicipios', data)
      let data_array = data["data"];
      this.next_page_url = data["next_page_url"]

      data_array.forEach(element => {
        if(this.item_selected){
          if(element.id == this.item_selected.id){
            //elemento salvo recebe true
            element.selected = true;
            this.municipio_array.push(element)
          } else {
            //demais elementos false
            element.selected = false;
            this.municipio_array.push(element)
          }
        } else {
          //todos os elementos recebem false
          element.selected = false;
          this.municipio_array.push(element)
        }

      });
      this.load.dismiss()
    }, err => {
      console.log('err', err)
      if(err){
        this.load.dismiss()
        let alert = this.alertCtrl.create({
          title: 'Erro',
          message: 'verifique sua conexão com a internet'
        })

        alert.present()
        
        setTimeout(() => {
          alert.dismiss()
          this.viewCtrl.dismiss()       
        }, 2000);
      }
    })
  }



  selectItem(item){
    console.log('item', item)
    this.multaProv.municipio_selected = item;

    this.viewCtrl.dismiss({ item });
  }

  

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    if(this.next_page_url){
      this.http.post(this.next_page_url + '&uf_emplacamento=' + this.uf_emplacamento, this.body).subscribe(data => {
        let new_array = data["data"]
        this.next_page_url = data["next_page_url"];
        console.log('next_page_url', this.next_page_url)
  
        new_array.forEach(element => {
          if(this.item_selected){
            if(element.id == this.item_selected.id){
              //elemento salvo recebe true
              element.selected = true;
              this.municipio_array.push(element)
            } else {
              //demais elementos false
              element.selected = false;
              this.municipio_array.push(element)
            }
          } else {
            //todos os elementos recebem false
            element.selected = false;
            this.municipio_array.push(element)
          }
          
        })
        infiniteScroll.complete();
      }, error => {
        console.log('error')
        infiniteScroll.complete();
      })
    } else {
      infiniteScroll.complete();
    }

  }

  

  getItems(ev: any) {
    // set val to the value of the searchbar
    const val = ev.target.value;

    console.log('val1', val)

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.view_loading = true;
      this.array_filter = 3;
      this.body_filter.nome = val;
      this.body_filter.uf_emplacamento = this.uf_emplacamento;
      this.multaProv.searchMunicipioByCode(this.body_filter).subscribe(data => {
        console.log('data["data"].length', data["data"].length > 0)
        if(data["data"].length > 0){
          console.log('dados filtrados', data)
          //his.array_filter = true
          this.municipio_array_filter = data["data"];
          this.municipio_array_filter_next_page = data["next_page_url"];
          console.log('this.enquadramento_array_filter_next_page = data["next_page_url"];', this.municipio_array_filter_next_page)
  
          this.municipio_array_filter.forEach(element => {
            if(this.item_selected){
              if(element.id == this.item_selected.id){
                //elemento salvo recebe true
                element.selected = true;
              } else {
                //demais elementos false
                element.selected = false;
              }
            } else {
              //todos os elementos recebem false
              element.selected = false;
            }
  
            this.array_filter = true;
            this.view_loading = false
          });
          
        } else {
          this.municipio_array_filter = []
          this.array_filter = true;
          this.view_loading = false
        }
      }, err => {
        console.log('err',err)
        this.array_filter = true;
        this.view_loading = false
      })
    } else {
      this.array_filter = false;
    }
  }
  
  getItemsDoInfinite(infiniteScroll) {

    if(this.municipio_array_filter_next_page !== null ){
      this.multaProv.searchMunicipioByCodeNextPage(this.municipio_array_filter_next_page, this.body_filter,  this.body ).subscribe(data => {
        let new_array = data["data"]
        this.municipio_array_filter_next_page = data["next_page_url"];
  
        new_array.forEach(element => {
          if(this.item_selected){
            if(element.id == this.item_selected.id){
              //elemento salvo recebe true
              element.selected = true;
              this.municipio_array_filter.push(element)
            } else {
              //demais elementos false
              element.selected = false;
              this.municipio_array_filter.push(element)
            }
          } else {
            //todos os elementos recebem false
            element.selected = false;
            this.municipio_array_filter.push(element)
          }
        })

        infiniteScroll.complete();
      }, error => {
        console.log('error')
        infiniteScroll.complete();
      })
    } else {
      infiniteScroll.complete();
    }

  }

  onCancel(){
    this.array_filter = false;
  }

 closeModal(){
  this.viewCtrl.dismiss()
}

loadingPresent(){
  this.load = this.loadingCtrl.create({
    content: 'Aguarde carregando dados'
  })

  this.load.present()
}



}
