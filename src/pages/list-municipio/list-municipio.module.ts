import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListMunicipioPage } from './list-municipio';

@NgModule({
  declarations: [
    ListMunicipioPage,
  ],
  imports: [
    IonicPageModule.forChild(ListMunicipioPage),
  ],
})
export class ListMunicipioPageModule {}
