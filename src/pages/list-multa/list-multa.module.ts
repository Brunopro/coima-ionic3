import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListMultaPage } from './list-multa';
import { ExpandableComponent } from '../../components/expandable/expandable'
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ListMultaPage,
    
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ListMultaPage)
  ],
  entryComponents: [
  ]
})
export class ListMultaPageModule {}
