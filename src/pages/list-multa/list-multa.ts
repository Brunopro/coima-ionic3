import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { PreferencesProvider } from '../../providers/preferences/preferences';
import { MultasProvider } from '../../providers/multas/multas';
import { AuthProvider } from '../../providers/auth/auth';

import moment from 'moment'

@IonicPage({
  defaultHistory: ['HomePage']
})
@Component({
  selector: 'page-list-multa',
  templateUrl: 'list-multa.html',
})
export class ListMultaPage {
  //isOpen = true;

  multas = [{placa:' ', data_infracao: ['00/00/0000 '], valor_recebido: '',  marca_e_modelo: '', observacao: '', expanded: false}]

  itemExpandHeight: number = 200;



  load: Loading;
  alterViewEmpityListVehicles: boolean;
  not_register_found: boolean;

  constructor(
    public navCtrl: NavController,
    public preferencesProv: PreferencesProvider,
    public loadingCtrl: LoadingController,
    public MultasProv: MultasProvider,
    public authProv: AuthProvider) {

    this.load = loadingCtrl.create({
      content: 'aguarde...'
    })

    this.load.present()

  }

  ionViewDidEnter() {

    //se tiver usuarios cadastrados
    if (this.preferencesProv.vehicle) {
      this.MultasProv.getMultas(this.preferencesProv.vehicle).subscribe(res => {
        console.log('res[0]', res)
        
        if (res[0] !== 'n') {
          this.load.dismiss()
          console.log('sem registros')
          this.multas = res
          let data = res[0]["data_infracao"].date.split(" ")
          console.log('data', moment(data[0]).format("DD/MM/YYYY")  )

          res.forEach((multa, i) => {

            console.log('foreach', multa)
            multa.data_infracao = multa.data_infracao.date.split(" ");
            multa.expanded = false;
            this.multas[i] = multa;
          });

          this.not_register_found = false;


          console.log('multas com expanded', this.multas)
        } else {
          this.load.dismiss()
          this.not_register_found = true;
          console.log('nenhum registro com esses dados encontrado')
        }
        
      }, error => {
        this.load.dismiss()
        this.not_register_found = true;
        console.log('não conseguimos conectar a api')
      })

      this.MultasProv.get().subscribe((res) => {
        console.log('get', res)
      }, err => console.log('err get teste', err))

      

    }
  }

  expandItem(item) {

    if(item.valor_recebido){
    console.log('item', item.valor_recebido.toFixed(2))
    }
    this.multas.map((listItem) => {

      if (item == listItem) {
        listItem.expanded = !listItem.expanded;
      } else {
        listItem.expanded = false;
      }

      return listItem;

    });

  }


}
