import { Injectable } from '@angular/core';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
import { SqliteHelperProvider } from '../sqlite-helper/sqlite-helper';
import { IEstado } from '../../interfaces/IEstado';
import { Platform } from 'ionic-angular';
import { ThrowStmt } from '@angular/compiler';

@Injectable()
export class EstadoProvider {
  constructor(
    private sqlite: SQLite,
    private platform: Platform
  ) { }

  private getDB(): Promise<SQLiteObject> {
    return this.platform.ready().then(() => {
      return this.sqlite.create({name: 'sqlite.db', location: 'default', createFromLocation: 1}).then((db:SQLiteObject) =>{ return db})
    });
  }

  getAll(orderBy?:string): Promise<IEstado[]> {
  
    return this.getDB()
      .then((db: SQLiteObject) => {
        console.log('getall estados db: ', db);
        return db.executeSql('select * from estados', [])
          .then((resultSet) => {
            let list: IEstado[] = [];
            for(let i = 0; i < resultSet.rows.length; i++){
              list.push(resultSet.rows.item(i))
            }
            console.log('Success getAll estados from SQLite resultSet', list);
            return list;
          }).catch((err: Error) => {
            console.error('Error executing method getAll!', err, err.message,err.name, err.stack);
            return Promise.reject(err.message || err);
          });
      })
  } 

}
