import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class WebServiceSoapMaProvider {

  private url:string = 'http://aossoftware.com.br/coima-api-multas/public/api';

  constructor(public http: HttpClient) {
    console.log('Hello WebServiceSoapMaProvider Provider');
  }

  consulta(params:{data:any, tipo:string, user:any}){


    if(params.data.tipo.name == 'Placa'){
      params.data.chave = params.data.chave.replace("-", "");
    }

    let data = {
      WS_PESQ: params.data.tipo.id,
      WS_CHAVE: params.data.chave.toUpperCase(),
      WS_TIPO: params.tipo
    }
    let body = {
      user:params.user,
      data:data
    }

    console.log('body consulta', body);
    return this.http.post(this.url + '/consultas', body)
  }

}
