import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { SqliteHelperProvider } from '../sqlite-helper/sqlite-helper';
import { IEnquadramento } from '../../interfaces/IEnquadramento';
import { environment } from '../../environments/environments';

@Injectable()
export class EnquadramentoProvider {

  private url:string = environment.url;

  public connection: boolean = true;
  private db: SQLiteObject;
  private isFirstCall: boolean = true;

  constructor(
    public http: HttpClient,
    public sqliteHelperProv: SqliteHelperProvider,
  ) {
  }

  searchEnquaByCode(code: number){

    let body = {
      data: code
    };

    console.log('data', body)

    let headers: HttpHeaders = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post<any[]>(this.url + '/enquadramentobycode', body,{headers: headers})
  
  }

  getApiAll(){
    let headers: HttpHeaders = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    
    return this.http.get<any[]>(this.url + '/enquadramento/all',{headers: headers})
  }


  private getDB(): Promise<SQLiteObject> {
    if (this.isFirstCall) {
      this.isFirstCall = false;
      return this.sqliteHelperProv.getDb('dynamicbox.db')
      .then((db: SQLiteObject) => {
        this.db = db;

        this.db.executeSql('CREATE TABLE IF NOT EXISTS enquadramentos (id INTEGER PRIMARY KEY,codigo INTEGER, descricao TEXT, desdobramento INTEGER)', [])
          .then(success => console.log('Enquadramentos table created successfully', success))
          .catch(err => console.error('Error creating enquadramentos table!', err));
        return this.db;
      })
    }

    return this.sqliteHelperProv.getDb();
  }

  createTable(): Promise<SQLiteObject> {
    return this.sqliteHelperProv.getDb('dynamicbox.db')
    .then((db: SQLiteObject) => {
      this.db = db;
      this.db.executeSql('CREATE TABLE IF NOT EXISTS enquadramentos (id INTEGER PRIMARY KEY,codigo INTEGER, descricao TEXT, desdobramento INTEGER)', [])
        .then(success => console.log('Enquadramentos table created successfully', success))
        .catch(err => console.error('Error creating enquadramentos table!', err));

      return this.db;
    });
  }

  getAll(orderBy?:string): Promise<IEnquadramento[]> {
    return this.getDB()
      .then((db: SQLiteObject) => {
        return this.db.executeSql('select * from enquadramentos', [])
          .then((resultSet) => {       
            let list: IEnquadramento[] = [];
            for(let i = 0; i < resultSet.rows.length; i++){
              list.push(resultSet.rows.item(i))
            }
            console.log('Success getAll enquadramentos from SQLite resultSet', list);
            return list;
          }).catch((err: Error) => {
            console.error('Error executing method getAll!', err, err.message,err.name, err.stack);
            return Promise.reject(err.message || err);
          });
      });
  } 

  create(enquadramento: IEnquadramento): Promise<IEnquadramento> {
    return this.db.executeSql('INSERT INTO enquadramentos (id,codigo, descricao, desdobramento) VALUES (?,?,?,?)', [enquadramento.id, enquadramento.codigo, enquadramento.descricao, enquadramento.desdobramento]).then(resultSet => {
      enquadramento.id = resultSet.insertId;
      return enquadramento;

    })
    .catch((err: Error) => {
      console.error(`Error creating '${enquadramento.codigo}' movie!`, err);
      return Promise.reject(err.message || err);
    });
  }

  dropTable(): Promise<any> {
    return this.db.executeSql('drop table if exists enquadramentos',[])
            .then((resultSet => resultSet ))
            .catch((err: Error) => {
              console.log('Error droping enquadramentos', err)
              return Promise.reject(err.message || err);
            })
  }


  delete(id:number): Promise<boolean> {
    return this.db.executeSql('delete from enquadramentos where id=?', [id])
            .then(resultSet => resultSet.rowsAffected > 0)
            .catch((err: Error) => {
              console.error(`Error deleting enquadramento with id=${id}`);
              return Promise.reject(err.message || err);
    });
  }







}
