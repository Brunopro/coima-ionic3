import { Injectable } from '@angular/core';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
import { SqliteHelperProvider } from '../sqlite-helper/sqlite-helper';
import { IMunicipio } from '../../interfaces/IMunicipio';
import { Platform } from 'ionic-angular';


@Injectable()
export class MunicipioProvider {

  constructor(
    private platform: Platform,
    private sqlite: SQLite
  ) { }

  private getDB(): Promise<SQLiteObject> {
    return this.platform.ready().then(() => {
      return this.sqlite.create({name: 'sqlite.db', location: 'default'})
    });
  }

  getAll(orderBy?:string): Promise<IMunicipio[]> {
    return this.getDB()
      .then((db: SQLiteObject) => {
        return db.executeSql('select * from municipios', [])
          .then((resultSet) => {
            let list: IMunicipio[] = [];
            for(let i = 0; i < resultSet.rows.length; i++){
              list.push(resultSet.rows.item(i))
            }
            console.log('Success getAll municipios from SQLite resultSet', list);
            return list;
          }).catch((err: Error) => {
            console.error('Error executing method getAll!', err, err.message,err.name, err.stack);
            return Promise.reject(err.message || err);
          });
      })
  }

  filterByEstate(estado_id:number): Promise<IMunicipio[]> {
    return this.getDB()
      .then((db: SQLiteObject) => {
        return db.executeSql(`select * from municipios where estado_id=${estado_id}`, [])
          .then((resultSet) => {
            let list: IMunicipio[] = [];
            for(let i = 0; i < resultSet.rows.length; i++){
              list.push(resultSet.rows.item(i))
            }
            console.log(`Success filter by estado_id=${estado_id} municipios from SQLite resultSet`, list);
            return list;
          }).catch((err: Error) => {
            console.error('Error executing method getAll!', err, err.message,err.name, err.stack);
            return Promise.reject(err.message || err);
          });
      })
  }

}
