import { Injectable } from '@angular/core';
import { AlertController, Loading, normalizeURL } from 'ionic-angular';
import { CameraOptions, Camera } from '@ionic-native/camera';




@Injectable()
export class CameraProvider {

  private base64Image:string;

  documento:any = {
    label: 'Foto 1', 
    valor: '',
    width: 0, 
    height: 0, 
    atributo: 'cnhF', 
    iconname: 'ios-add'
  }

  load:Loading;


  constructor(
    private alertCtrl: AlertController,
    private camera:Camera) {
    console.log('Hello CameraProvider Provider');
  }

  takePictureGalery(load:Loading):Promise<any> {

    this.load = load;

    return new Promise((resolve, reject) => {
      this.temPermissao().then(
        res => {
          if(res) {
            this.escolherFoto().then(  
              res => {

                console.log('img.data', res.data)
                this.documento.valor = res.data
                this.documento.width = res.width
                this.documento.height = res.height
                this.documento.iconname = 'ios-checkmark'
                this.load.dismiss();
                resolve(res);
              },
              err => {
                load.dismiss();
                reject(err)
              }
            )
          } else {
            this.pedirPermissao().then(
              res => {
                console.log('RESULTADO DO PEDIDO ')
                this.load.dismiss()
              },
              err => {
                this.load.dismiss()
                let alert = this.alertCtrl.create({
                  title: 'Autorização',
                  message: 'Este app precisa que você autorize o acesso à biblioteca de fotos nas configurações do dispositivo.',
                  buttons: [{text:'Ok'}]
                })

                alert.present()
              }
            )
          }
        }
      )
    })

  }


  pedirPermissao() {
    return new Promise((resolve,reject) => {
      window['imagePicker'].requestReadPermission(
        res => { resolve(res) },
        err => { reject(err) }
      )
    })
  }


  temPermissao():Promise<boolean> {
    return new Promise((resolve) => {
      window['imagePicker'].hasReadPermission(
        res => { resolve(res) }
      )
    })
  }

  escolherFoto():Promise<any> {
    let options = {
      maximumImagesCount: 1,
      outputType: 1,
      quality: 98,
      width: 800,
      title:'Escolha uma imagem'
    }

    return new Promise((resolve,reject) => {
      window['imagePicker'].getPictures(
        res => {
          if(res.length > 0){

            this.getImageDimensions("data:image/jpg;base64," + res[0]).then(
              result => {
                let img = {
                  //data:"data:image/jpg;base64," + res[0],
                  data: res[0],
                  width: result['w'],
                  height: result['h']
                }
                console.log('WxH: ' + result['w'] + 'x' + result['h'] + 'imagebase64' + res[0]) 
                resolve(res[0])
              },
              err=>{console.log('ERRO Dimensao ' + err)}
            )
          }
          else { reject('Sem imagem selecionada') }
        },
        err => { reject('Erro durante a escolha da imagem') }, options)
      })
    }

    getImageDimensions(file) {
      return new Promise (function (resolved, rejected) {
        var i = new Image()
        i.onload = function(){
          resolved({w: i.width, h: i.height})
        };
        i.src = file
      })
    }

    takePictureCamera(load:Loading):Promise<String>{
  
      return new Promise((resolve, reject) => {
        let opcoesDeCameraLibrary: CameraOptions = {
          cameraDirection: 0,
          correctOrientation: false,
          destinationType: 0,
          encodingType: 1,
          quality: 80,
          saveToPhotoAlbum: true,
          sourceType: 1,
          targetWidth: 800
        }
      
        
        this.camera.getPicture(opcoesDeCameraLibrary).then((imageData) => {
         // imageData is either a base64 encoded string or a file URI
         // If it's base64 (DATA_URL):
         this.base64Image = 'data:image/png;base64,' + imageData;
         this.base64Image = normalizeURL(this.base64Image);
    
         this.encodeImageUri(this.base64Image, function (image64) {
           console.log('image64   apos encodeImageUri',image64)
           image64 = image64.replace('data:image/jpeg;base64,', '');
           console.log('image64 replace',image64)
           load.dismiss();
           resolve(image64)
         })
    
    
        }, err => {
         // Handle error
         load.dismiss();
         reject(err);
        });
      })
      
    }
  
     //onTakePicture and openImagePickerCrop
     encodeImageUri(imageUri, callback) {
      console.log('execute encodeImageUri')
      var c = document.createElement('canvas');
      var ctx = c.getContext("2d");
      var img = new Image();
      img.onload = function () {
        var aux: any = this;
        c.width = aux.width;
        c.height = aux.height;
        ctx.drawImage(img, 0, 0);
        var dataURL = c.toDataURL("image/jpeg");
        callback(dataURL);
      };
      img.src = imageUri;
    };



  
}
