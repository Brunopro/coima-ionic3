import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';
import { IUser } from '../../interfaces/IUser';
import { Platform } from 'ionic-angular';
@Injectable()
export class PreferencesProvider {

  user: IUser;

  vehicle = [];

  enquadramento_selected:any;

  constructor(
    private storage:Storage,
    private platform: Platform
    ) {

  }

  create(key, value): Promise<any>  {
    return this.storage.ready()
    .then(() => { 
      return this.storage.set(key, value)
        .then(() => {
          if(key == 'vehicle'){
            this.vehicle = value;
          } else if (key == 'user'){
            this.user = value;
          }
        })
    })  
  }

  //Vai trazer o user com os dados setados
  get(key): Promise<any> {
    return this.storage.ready()
    .then(() => { 
      return this.storage.get(key).then((value) => {
        if('user'){
          this.user = value;
        } else if('vehicle'){
          this.vehicle = value;
        }

        return value;
      });
    })
  }

  // Quando deslogar deve remover do storage
  //usar o preference para deslogar
  remove(key): Promise<boolean> {
    return this.storage.ready()
      .then(() => {
        return this.storage.remove(key)
        .then(() => {
          return true
        })
      })
  }

}
