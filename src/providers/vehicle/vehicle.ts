import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { PreferencesProvider } from '../preferences/preferences';
import moment from 'moment'

@Injectable()
export class VehicleProvider {

  constructor(
    public afdb: AngularFireDatabase,
    public preferencesProv: PreferencesProvider
  ) {
    console.log('Hello VehicleProvider Provider');
  }


  addVehicleInDatabase(vehicle, self){
    console.log('addVehicleInDatabase', vehicle.placa, '  ', vehicle.renavan)

    
    let newVehicleRef = this.afdb.database.ref('users').child(this.preferencesProv.user.uid).child('vehicle').push();
    self.uid = newVehicleRef.key;
    return newVehicleRef.set({
      placa: vehicle.placa,
      renavan: vehicle.renavan,
      create_at: moment().format('L') + ' ' + moment().format("HH:mm:ss"),
    })
  }

  deleteVehicle(vehicle){
    console.log(vehicle)
   return this.afdb.database.ref('users').child(this.preferencesProv.user.uid).child('vehicle').child(vehicle.uid).remove()
  }

  updateVehicle(vehicle){
    return this.afdb.database.ref('users').child(this.preferencesProv.user.uid).child('vehicle').child(vehicle.uid).update({
      placa: vehicle.placa,
      renavan: vehicle.renavan
    })  }

}
