import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Checkbox } from 'ionic-angular';
import { SQLiteObject } from '@ionic-native/sqlite';
import { SqliteHelperProvider } from '../sqlite-helper/sqlite-helper';
import { IMulta } from '../../interfaces/IMulta';
import { environment } from '../../environments/environments';

@Injectable()
export class MultasProvider {

  enquadramento_selected = {
    id: '',
    codigo: '',
    descricao: '',
    desdobramento: ''
  }

  municipio_selected = {
    id: '',
    nome: '',
    codigo: ''
  };
  

  private db: SQLiteObject;
  private isFirstCall: boolean = true;

  //Verificar se a internet está ativa
  public connection:boolean;

  current_checkbox_municipio: Checkbox;
  current_checkbox_enquadramento: Checkbox;

  private url:string = environment.url;


  constructor(
    public http: HttpClient,
    public sqliteHelperProv: SqliteHelperProvider
    ) {
    console.log('Hello MultasProvider Provider');
  }

  get(){
    return this.http.get(this.url + '/teste')
  }
  
  saveMultaInApi(multas:IMulta[], user){
    let body = {
      multas: multas,
      user: user
    }

    console.log('body saveMultaInApi', body);
    return this.http.post<any>(this.url + '/setmulta', body);
  }

  getMultas(vehicles){

    let data = [];
    data.push(vehicles)

    console.log('data', data)

    let headers: HttpHeaders = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post<any[]>(this.url + '/multas', data,{headers: headers})
  
  }

  getMunicipios(body){
    console.log('body', body)
    return this.http.post<any[]>(this.url + '/municipios', body)
  }

  searchMunicipioByCode(body){
    return this.http.post<any[]>(this.url + '/municipiobyname', body)
  }

  searchMunicipioByCodeNextPage(next_page_url, data, body){
    return this.http.post<any[]>(next_page_url + '&uf_emplacamento=' + data.uf_emplacamento + '&nome=' + data.nome, body)
  }

  getAllMultasByAgente(user){
    let headers: HttpHeaders = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    let body = {
      user: user
    }
    //enviando user para teste
    return this.http.post<any>(this.url + '/agente/multas', body, {headers:headers})
  }

  public updateDadosCadastrais(body:{admin:number, cpf:string}){
    return this.http.post(this.url + '/auth/getalldata', body)
  }

  getMoreMultas(next_page_url, cpf){
    let body = {
      cpf:cpf
    }

    let headers: HttpHeaders = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<any>(next_page_url + `&cpf=${cpf}`, cpf, {headers:headers});
  }

  updateMulta(multa_updated, user){

    let body = {
      multa: multa_updated,
      user:user
    }
    return this.http.put(this.url + '/multas', body)
  }

  deleteMulta(id, user){
    let body = {
      id: id,
      user: user
    }
    return this.http.post(this.url +  '/multas/delete', body)
  }

  getAgenteByCpf(body){
    return this.http.post(this.url + '/agentes/cpf', body)
  }

  deleteInApi(id, user){
  
    let body = {
      id: id,
      user: user
    }

    return this.http.post(this.url + '/multas/delete', body)
  }

  public getDB(): Promise<SQLiteObject> {
    if (this.isFirstCall) {
      this.isFirstCall = false;
      return this.sqliteHelperProv.getDb('dynamicbox.db', true)
      .then((db: SQLiteObject) => {
        this.db = db;

        this.db.executeSql('CREATE TABLE IF NOT EXISTS multas (' +
          'id INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,' + 
          'multa_id INTEGER, ' + 
          'enviado  INTEGER, ' +  
          'agentes_id INTEGER,' +
          'enquadramento_id INTEGER,' +
          'estado_id INTEGER,' +
          'status INTEGER,' +
          'uf_emplacamento_id INTEGER,' +
          'municipio_emplacamento_id INTEGER,' +
          'uf_emplacamento TEXT,' +
          'created TEXT,' + 
          'agente_nome TEXT,' + 
          'data_infracao TEXT,' + 
          'data_infracao_br TEXT,' + 
          'descricao_marca_modelo TEXT,' + 
          'enquadramento_codigo TEXT,' + 
          'enquadramento_descricao TEXT,' + 
          'enquadramento_desdobramento TEXT,' + 
          'foto_infracao TEXT,' + 
          'hora_infracao TEXT,' + 
          'local_ocorrencia TEXT,' + 
          'municipio_emplacamento_nome TEXT,' + 
          'numero_auto_infracao TEXT,' + 
          'observacao TEXT,' + 
          'placa TEXT)', [])
          .then(success => console.log('Multas table created successfully', success))
          .catch(err => console.error('Error creating multas table!', err));
        return this.db;
      })
    }

    return this.sqliteHelperProv.getDb();
  }

  getAll(orderBy?:string): Promise<IMulta[]> {
    return this.getDB()
      .then((db: SQLiteObject) => {
        return db.executeSql('select * from multas order by created desc', [])
          .then((resultSet) => {       
            let list: IMulta[] = [];
            for(let i = 0; i < resultSet.rows.length; i++){
              list.push(resultSet.rows.item(i))
            }
            console.log('Success getAll multas from SQLite resultSet', list);
            return list;
          }).catch((err: Error) => {
            console.error('Error executing method getAll!', err, err.message,err.name, err.stack);
            return Promise.reject(err.message || err);
          });
      });
  }

  getCondition(query:string):Promise<IMulta[]> {
    return this.getDB()
    .then((db: SQLiteObject) => {
      return db.executeSql(query, [])
        .then((resultSet) => {       
          let list: IMulta[] = [];
          for(let i = 0; i < resultSet.rows.length; i++){
            list.push(resultSet.rows.item(i))
          }
          console.log('Success getCondition multas from SQLite resultSet', list);
          return list;
        }).catch((err: Error) => {
          console.error('Error executing method getCondition!', err, err.message,err.name, err.stack);
          return Promise.reject(err.message || err);
        });
        
    });
  }

  create(multa: IMulta): Promise<IMulta> {
    return this.getDB()
    .then((db: SQLiteObject) => {
    return db.executeSql(
      'INSERT INTO multas (' +
        'multa_id, ' + 
        'enviado, ' + 
        'agentes_id,' +
        'enquadramento_id,' + 
        'estado_id,' +
        'status,' +
        'uf_emplacamento_id,' +
        'municipio_emplacamento_id,' +
        'uf_emplacamento ,' +
        'created ,' +
        'agente_nome ,' +
        'data_infracao ,' +
        'data_infracao_br ,' +
        'descricao_marca_modelo ,' +
        'enquadramento_codigo ,' +
        'enquadramento_descricao ,' +
        'enquadramento_desdobramento ,' +
        'foto_infracao ,' +
        'hora_infracao ,' +
        'local_ocorrencia ,' +
        'municipio_emplacamento_nome ,' +
        'numero_auto_infracao ,' +
        'observacao ,' +
        'placa,' +

        'enquadramento_codigo,' +
        'agente_nome ,' +
        'enquadramento_descricao ,' +
        'enquadramento_desdobramento ,' +
        'estado_id ,' +
        'municipio_emplacamento_nome ,' +
        'uf_emplacamento' +
        ')' + 
      'VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
        multa.multa_id,
        multa.enviado,
        multa.agentes_id,
        multa.enquadramento_id,
        multa.estado_id,
        multa.status,
        multa.uf_emplacamento_id,
        multa.municipio_emplacamento_id,
        multa.uf_emplacamento,
        multa.created,
        multa.agente_nome,
        multa.data_infracao,
        multa.data_infracao_br,
        multa.descricao_marca_modelo,
        multa.enquadramento_codigo,
        multa.enquadramento_descricao,
        multa.enquadramento_desdobramento,
        multa.foto_infracao,
        multa.hora_infracao,
        multa.local_ocorrencia,
        multa.municipio_emplacamento_nome,
        multa.numero_auto_infracao,
        multa.observacao,
        multa.placa,
        multa.enquadramento_codigo,
        multa.agente_nome,
        multa.enquadramento_descricao,
        multa.enquadramento_desdobramento,
        multa.estado_id,
        multa.municipio_emplacamento_nome,
        multa.uf_emplacamento
      ]
      ).then(resultSet => {
      multa.id = resultSet.insertId;
      return multa;
      
      });
    })
    .catch((err: Error) => {
      console.error(`Error creating '${multa.id}' multa!`, err);
      return Promise.reject(err.message || err);
    });
  }

  dropTable(): Promise<any> {
    return this.db.executeSql('drop table if exists multas',[])
            .then((resultSet => resultSet ))
            .catch((err: Error) => {
              console.log('Error droping multas', err)
              return Promise.reject(err.message || err);
            })
  }

  count(): Promise<any> {
    return this.getDB()
      .then((db: SQLiteObject) => {
        return db.executeSql('select count(*) from multas where enviado=1', [])
          .then((resultSet) => {       
            console.log('Success count multas from SQLite resultSet', resultSet.rows.item(0)["count(*)"]);
            return resultSet.rows.item(0)["count(*)"];
          }).catch((err: Error) => {
            console.error('Error executing method getAll!', err, err.message,err.name, err.stack);
            return Promise.reject(err.message || err);
          });
      });
  } 

  delete(id:number): Promise<boolean> {
    console.log('id multa delete', id)
    return this.getDB()
      .then((db: SQLiteObject) => {
        return db.executeSql('delete from multas where id=?', [id])
            .then(resultSet => resultSet.rowsAffected > 0)
            .catch((err: Error) => {
              console.error(`Error deleting multas with id=${id}`);
              return Promise.reject(err.message || err);
      });
    });
  }

  
  deleteMultaInApi(id,user){
  
    let body = {
      id: id,
      user:user
    }

    return this.http.post(this.url + '/multas/delete', body)
  }

  async deleteDuplicateMultas(multas:IMulta[]):Promise<any>{
      for (const [idx, multa] of multas.entries()) {
        console.log('idx', idx, 'multa', multa)
        const todo = await this.db.executeSql('delete from multas where id=?', [multa["id"]])
        .then(resultSet => {
          
          resultSet.rowsAffected > 0;
          return Promise.resolve(resultSet.rowsAffected);
        })
        .catch((err: Error) => {
          console.error(`Error deleting multas with id=${multa["id"]}`);
          return Promise.reject(err.message || err);
        });
        console.log(`Received Todo ${idx+1}:`, todo);
      }
    
      console.log('Finished!');
  }
  


  
}
