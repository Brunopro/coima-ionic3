import { Injectable } from '@angular/core';
import { ToastController, LoadingController, AlertController, Loading } from 'ionic-angular';

@Injectable()
export class OverlayProvider {

  constructor(
    private toastCtrl:ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {
  }

  public toastPresent(opts:{message:string, duration?:number, closeButtonText?: string}):void{
    this.toastCtrl.create({
      showCloseButton: true,
      duration: opts.duration? opts.duration : 3000,
      dismissOnPageChange: true,
      closeButtonText: opts.closeButtonText? opts.closeButtonText : 'OK',
      message: opts.message
    }).present();
  }

  public loadPresent(content?:string):Loading{
    let load = this.loadingCtrl.create({
      content: content? content: 'aguarde...'
    })

    load.present();

    return load;
  }

  public alertPresent(opts:{title?:string, message:string}):void{
    this.alertCtrl.create({
      title: opts.title? opts.title: 'Alerta',
      message: opts.message,
      buttons: ['OK']
    }).present();
  }

  

}
