import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
import { SqliteHelperProvider } from '../sqlite-helper/sqlite-helper';
import { IAgente } from '../../interfaces/IAgente';

@Injectable()
export class AgenteProvider {

  private db: SQLiteObject;
  private isFirstCall: boolean = true;

  constructor(
    private sqliteHelperProv: SqliteHelperProvider
  ) { }

  private getDB(): Promise<SQLiteObject> {
    if (this.isFirstCall) {
      this.isFirstCall = false;
      return this.sqliteHelperProv.getDb('dynamicbox.db')
      .then((db: SQLiteObject) => {
        this.db = db;
        this.db.executeSql('CREATE TABLE IF NOT EXISTS agentes (id INTEGER PRIMARY KEY, nome TEXT, cpf TEXT)', [])
          .then(success => console.log('Agentes table created successfully', success))
          .catch(err => console.error('Error creating agentes table!', err));
        return this.db;
      })
    }
    return this.sqliteHelperProv.getDb();
  }

  getAll(orderBy?:string): Promise<IAgente[]> {
    return this.getDB()
      .then((db: SQLiteObject) => {
        return db.executeSql('select * from agentes', [])
          .then((resultSet) => {
            let list: IAgente[] = [];
            for(let i = 0; i < resultSet.rows.length; i++){
              list.push(resultSet.rows.item(i))
            }
            console.log('Success getAll agentes from SQLite resultSet', list);
            return list;
          }).catch((err: Error) => {
            console.error('Error executing method getAll!', err, err.message,err.name, err.stack);
            return Promise.reject(err.message || err);
          });
      })
  } 

  getAgenteByCpf(cpf:string): Promise<any> {
    return this.getDB()
    .then((db: SQLiteObject) => {
      return this.db.executeSql('select * from agentes where cpf=?', [cpf])
      .then((resultSet) => {
        console.log('resultSet.rows.item(0) :', resultSet.rows.item(0))
        return resultSet.rows.item(0);
      }).catch((err: Error) => {
        console.error(`Error select agente where cpf = ${cpf}`, err, err.message,err.name, err.stack);
        return Promise.reject(err.message || err);
      })
    })
  }

  create(agente: IAgente): Promise<IAgente> {
    return this.db.executeSql('INSERT INTO agentes (id,uf,cpf) VALUES (?,?,?)', [agente.id, agente.nome, agente.cpf])
      .then(() => {
      return agente;
    })
    .catch((err: Error) => {
      console.error(`Error creating '${agente.nome}' agente!`, err);
      return Promise.reject(err.message || err);
    });
  }

  dropTable(): Promise<any> {
    return this.db.executeSql('drop table if exists agentes',[])
      .then((resultSet => {
        console.log('table agentes droped', resultSet);
        return resultSet
      }))
      .catch((err: Error) => {
        console.log('Error droping agentes', err)
        return Promise.reject(err.message || err);
      })
  }

}
