import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import moment from 'moment'
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environments';

@Injectable()
export class AuthProvider {

  private url:string = environment.url;
  uid: any;

  constructor(
    private auth: AngularFireAuth,
    private afd: AngularFireDatabase,
    public http: HttpClient) {

  }

  signIn(email, password) {
    return this.auth.auth.signInWithEmailAndPassword(email, password)
  }

  singUp(user) {

    return this.verifyCpf(user.cpf).then(()  => {
      return this.auth.auth.createUserWithEmailAndPassword(user.email, user.password).then((data) => {

        
        
        this.uid =  data.user.uid


        console.log('this.res', this.uid)

        return this.afd.database.ref('users').child(this.uid).set({
          admin: 0,
          company: '',
          cpf: user.cpf,
          email: user.email.toLowerCase(),
          name: user.name.toLowerCase(),
          lastname: user.lastname.toLowerCase(),
          phone: user.phone,
          create_at: moment().format('L') + ' ' + moment().format("HH:mm:ss"),
          update_at: ''
        })

        
      })
    })

  }

  //Esqueceu a senha
  resetPassword(email: string): any {
    return this.auth.auth.sendPasswordResetEmail(email)
  }

  logout(){
    this.auth.auth.signOut()
  }
  

  verifyCpf(cpf): Promise<any> {

    return new Promise((resolve, reject) => {
      this.afd.database.ref('users').orderByChild('cpf').equalTo(cpf).on("value", function (snapshot) {
        if (snapshot.exists()) {
          reject('cpf exist')
        } else {
         let err = {
            code: 'cpf',
            message: 'cpf exist'
          }
          resolve(err)
        }
      })
    })
  }

  getDataUserInDatabase(uid):Promise<any>{
    console.log('uid', uid)
    return new Promise((resolve, reject ) => {
      this.afd.database.ref('users').child(uid).on('value', (snapshot) => {
        console.log(snapshot.val())
        if(snapshot.exists()){
          resolve(snapshot.val())
        } else {
          reject('erro/datanotfound')
        }
      })
    })
  }

  setDataVehicle(uid, data_vehicle){
    data_vehicle.placa = data_vehicle.placa.toUpperCase();
    console.log('Placa Upper Case', data_vehicle.placa)
    return this.afd.database.ref('users').child(uid).child('vehicle').push({
      placa: data_vehicle.placa,
      renavan: data_vehicle.renavan ,
      create_at: moment().format('L') + ' ' + moment().format("HH:mm:ss"),
    })
  }

  getDataVehicleInDatabase(uid):Promise<any>{
    return new Promise((resolve, reject ) => {
      this.afd.database.ref('users').child(uid).child('vehicle').on('value', (snapshot) => {
        console.log(snapshot.val())
        if (snapshot.val() != null) {
          
          let vehicles = []
          Object.keys(snapshot.val()).map(key => {
            let vehicle = snapshot.val()[key];
            vehicle.uid = key;
            vehicles.push(vehicle)
          });

          console.log('vehicles', vehicles)
          
          
          
          
          resolve(vehicles)
              
        }
  
      })
    })
  }

  verifyAdmin(body){
    return this.http.post(this.url + '/agentes/verificacao', body)
  }

}
