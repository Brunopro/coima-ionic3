import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ValidatorProvider } from '../providers/validator/validator';
import { AuthProvider } from '../providers/auth/auth';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { PreferencesProvider } from '../providers/preferences/preferences';
import { IonicStorageModule } from '@ionic/storage';
import { MultasProvider } from '../providers/multas/multas';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Geolocation } from '@ionic-native/geolocation';

import { GoogleMaps } from '@ionic-native/google-maps';
import { Diagnostic } from '@ionic-native/diagnostic';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { CameraProvider } from '../providers/camera/camera';
import { Camera } from '@ionic-native/camera';
import { VehicleProvider } from '../providers/vehicle/vehicle';

import { IonicSelectableModule } from 'ionic-selectable';
import { EnquadramentoProvider } from '../providers/enquadramento/enquadramento';
import { ComponentsModule } from '../components/components.module';

import { LongPressModule } from 'ionic-long-press';

import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { SQLite } from '@ionic-native/sqlite';
import { SqliteHelperProvider } from '../providers/sqlite-helper/sqlite-helper';
import { MunicipioProvider } from '../providers/municipio/municipio';
import { AgenteProvider } from '../providers/agente/agente';
import { EstadoProvider } from '../providers/estado/estado';

import { Network } from '@ionic-native/network';
import { AppVersion } from '@ionic-native/app-version';

import { WebServiceSoapMaProvider } from '../providers/web-service-soap-ma/web-service-soap-ma';
import { OverlayProvider } from '../providers/overlay/overlay';

export const firebase = {
  apiKey: "AIzaSyAs7RxvSVgOXOj52a3LQvWMD77JmsEdDic",
  authDomain: "coima-a18ca.firebaseapp.com",
  databaseURL: "https://coima-a18ca.firebaseio.com",
  projectId: "coima-a18ca",
  storageBucket: "coima-a18ca.appspot.com",
  messagingSenderId: "43251050141"
};

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    ComponentsModule,
    LongPressModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    
    IonicModule.forRoot(MyApp, {
      preloadModules: true
    }),
    AngularFireModule.initializeApp(firebase),
    AngularFireDatabaseModule,
    IonicSelectableModule,
 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpClient,
    ValidatorProvider,
    AuthProvider,
    AngularFireAuth,
    PreferencesProvider,
    MultasProvider,
    GoogleMaps,
    Geolocation,
    Diagnostic,
    NativeGeocoder,
    Camera,
    CameraProvider,
    VehicleProvider,
    EnquadramentoProvider,
    BluetoothSerial,
    ImagePicker,
    SQLite,
    SqliteHelperProvider,
    MunicipioProvider,
    AgenteProvider,
    EstadoProvider,
    Network,
    WebServiceSoapMaProvider,
    OverlayProvider,
    AppVersion
    
  ]
})
export class AppModule {}
