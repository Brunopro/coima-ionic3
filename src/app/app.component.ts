import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, AlertController, ToastController, Loading, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PreferencesProvider } from '../providers/preferences/preferences';
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../providers/auth/auth';
import { Network } from '@ionic-native/network';
import { MultasProvider } from '../providers/multas/multas';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
import { SqliteHelperProvider } from '../providers/sqlite-helper/sqlite-helper';
import { NodeData } from '@angular/core/src/view';
import { IMulta } from '../interfaces/IMulta';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{ title: string, component: any }>;
  pagesAdmin: Array<{ title: string, component: any }>;
  load: Loading;
  user:any;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public preferencesProv: PreferencesProvider,
    private storage: Storage,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public authProv: AuthProvider,
    public network: Network,
    public multasProv: MultasProvider,
    public sqliteHelperProv: SqliteHelperProvider,
    public sqlite: SQLite,
    public http: HttpClient,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController

  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Minhas multas', component: 'ListMultaPage' },
      { title: 'Meus veículos', component: 'MyVehiclePage' }
    ];

    this.pagesAdmin = [
      /*{ title: 'Minhas multas', component: 'ListMultaPage' },*/
      /*{ title: 'Meus veículos', component: 'MyVehiclePage' },*/
      { title: 'Multas', component: 'ListCreatedMultasPage' },
      { title: 'Consultar Condutor', component: 'ConsultaCondutorPage'},
      { title: 'Consultar Veiculo', component: 'ConsultaVeiculoPage'}
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      
      this.storage.ready().then(() => {
        this.preferencesProv.get('user')
          .then((user) => {
            this.user = user;
            if (user) {
              this.preferencesProv.get('vehicle')
                .then((vehicle) => {
                  this.preferencesProv.vehicle = vehicle
                  if (this.network.type != 'none') {
                    console.log('com internet!')
                    this.updateDataSqlite()
                    this.multasProv.connection = true;
                  } else {
                    console.log('sem internet')
                    this.multasProv.connection = false;
                  }
                  console.log('user', user)
                  if (user.admin == 1) {
                    console.log("é admin")
                    this.enableAuthenticatedMenu('admin').then(() => {
                      this.rootPage = 'HomePage'
                      this.hideSplashscreen()
                    })
                  } else {
                    console.log("não é admin")
                    this.enableAuthenticatedMenu('users').then(() => {
                      this.rootPage = 'HomePage'
                      this.hideSplashscreen()
                    })
                  }
                })
            } else {
              this.hideSplashscreen()
              this.rootPage = 'SignInPage'
            }
          })
      })
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.backgroundColorByHexString('#51a39a');
      this.subscribeNetworkConnection();
    });
  }

  private hideSplashscreen(): void {
    if (this.splashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 500);
    }
  }

  subscribeNetworkConnection() {
    // watch network for a disconnect
    this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      this.multasProv.connection = false;
    });

    // watch network for a connection
    this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        this.multasProv.connection = true;
        //this.saveDataOfflineIfExists();          
      }, 3000);
    });
  }

  /*
  saveDataOfflineIfExists(){
    this.movieProv.getAll().then((movies: Movie[]) => {
      if(movies.length > 0){
        movies.forEach(element => {
          if(element.status == 0){
            console.log('gravando cadastros feitos offline no firebase');
            this.movieProv.createFirebase(element).then((uid) => {
              element.status = 1;
              element.uid = uid;
              this.movieProv.update(element)
            }).catch((err: Error) => {
              console.log('erro ao inserir dados gravador offline no banco', err)
            })
          }
        });
      } else {
        console.log('Sem registros offline para gravar no firebase');
      }
    });
  }
  */

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logoutConfirm() {
    this.menuCtrl.close()
    let confirm = this.alertCtrl.create({
      title: 'Confirmação',
      message: 'Tem certeza que deseja deslogar dessa conta?',
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            //trata a promisse e manda o data chamando o setMobileAtual
            this.logout();
          }
        },
        {
          text: 'Cancelar',
          handler: () => {

          }
        }
      ]
    });
    confirm.present();
  }

  logout() {
    console.log('logout')
    this.platform.ready().then(() => {
      this.preferencesProv.get('user').then((user) => {
        if(user.admin == 1){
        this.sqlite.deleteDatabase({ name: 'dynamicbox.db', location: 'default' })
        .then(() => {
          console.log('banco destruido')
          this.preferencesProv.remove('user').then(() => {
            this.preferencesProv.user = null;
            this.preferencesProv.remove('vehicle').then(() => {
              this.preferencesProv.vehicle = null;
              this.authProv.logout()
              this.nav.setRoot('SignInPage');
            })
          })
        })
        } else {
          this.preferencesProv.remove('user').then(() => {
            this.preferencesProv.user = null;
            this.preferencesProv.remove('vehicle').then(() => {
              this.preferencesProv.vehicle = null;
              this.authProv.logout()
              this.nav.setRoot('SignInPage');
            })
          })
        }
      });
    })
  }

  enableAuthenticatedMenu(user: string): Promise<void> {
    return new Promise(resolve => {
      if (user == 'users') {
        this.menuCtrl.enable(true, 'users');
        this.menuCtrl.enable(false, 'admin');
        return resolve();
      } else {
        this.menuCtrl.enable(false, 'users');
        this.menuCtrl.enable(true, 'admin');
        return resolve()
      }
    })
  }

  private updateDataSqlite():void {

    this.multasProv.getCondition('select * from multas where enviado = 0')
      .then((multas_offline:IMulta[]) => {
        console.log('multas_offline', multas_offline)

        if(multas_offline.length > 0){
          this.loadPresent()
          //mandar user
          let user = {
            cpf: this.user.cpf.replace(".", "").replace(".", "").replace("-", ""),
            company: this.user.company
          }
          console.log('existe multas gravadas e não enviadas')
          this.multasProv.saveMultaInApi(multas_offline, user).subscribe(async(res) => {
            console.log('res saveMultaInApi', res)
  
            if(res){
              let multas_salvas = [];
              multas_salvas = res["multas_salvas"];
              let multas_existentes:IMulta[];
              multas_existentes = res["multas_existentes"];
              console.log('multas', multas_salvas)
              if(multas_salvas.length > 0){
  
                let array_query = [];
                for (let i = 0; i < multas_salvas.length; i++) {
  
                  let created:string = multas_salvas[i]["created"];
                  array_query.push([
                    `DELETE FROM multas where id = ${multas_salvas[i]["id_multa_sqlite"]}`, []
                  ]);
  
                  console.log('array_query multas salvas', array_query)
                  if(i == (multas_salvas.length -1)){
                    console.log('atualizando multas sqlite')
                    this.multasProv.getDB().then((db:SQLiteObject) => {
  
                      console.log('array_query', array_query, 'db ', db)
                      db.sqlBatch(array_query)
                        .then(() => {
                          this.load.dismiss();
                          console.log('multas envidas excluidas do sqlite com sucesso')
                        }).catch((e) => {
                          this.load.dismiss();
                          console.log('erro ao excluir multas enviadas do sqlite',e)
                        })
                    })
                  }
                }

                if(multas_existentes.length > 0){
                  this.multasInvalidas(multas_existentes);
                }
                this.toastCtrl.create({
                          message: `Multas enviadas: salvas com sucesso (${multas_salvas.length}), falha ao salvar (${multas_existentes.length}) numero auto infração duplicado`,
                          duration: 20000,
                          showCloseButton:true,
                          closeButtonText: 'OK',
                          dismissOnPageChange: true
                }).present()

              } else {
                if(multas_existentes.length > 0){
                console.log('contem multas no sql duplicadas que não podem ser salvas')
                this.multasInvalidas(multas_existentes).then((result) => {
                  this.load.dismiss();
                  console.log('result delete', result)
                }).catch((e) => {
                  console.error('error delte', e);
                  this.load.dismiss()
                })
                this.toastCtrl.create({
                  message: `Multas enviadas: salvas com sucesso (${multas_salvas.length}), falha ao salvar (${multas_existentes.length}) numero auto infração duplicado`,
                  duration: 20000,
                  showCloseButton:true,
                  closeButtonText: 'OK',
                  dismissOnPageChange: true
                }).present()
                }
              }

            }
          }, err => {
            this.load.dismiss()
            console.log('error ao savar multas não enviadas a api', err);
          })
        } else {
          console.log('Sem multas do sqlite para serem enviadas')
        }
      })
  }

  loadPresent() {
    this.load = this.loadingCtrl.create({
      content: 'Atualizando dados'
    })

    this.load.present();
  }


  multasInvalidas(multas_existentes):Promise<any>{
    console.log('multas_existentes', multas_existentes);
    return this.multasProv.deleteDuplicateMultas(multas_existentes);
  }


}
